//
//  ActorDetailMovieTableViewCell.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 17/09/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import UIKit

class ActorDetailMovieTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var viewModel: ActorDetailViewModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        viewModel = ActorDetailViewModel()
        
        let nib = UINib(nibName: "MovieCollectionViewCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "MovieCollectionCell")
    }
    
}

extension ActorDetailMovieTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieCollectionCell", for: indexPath) as? MovieCollectionViewCell else {
            
            print("Creating empty collection view cell")
            return UICollectionViewCell()
        }
        
        if let movie = viewModel.actor?.knownFor[indexPath.row] {
            cell.setupWithData(data: movie)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard
            let actor = viewModel.actor
        else {
            print("no actor found")
            return 0
        }
        
        return actor.knownFor.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let id = viewModel.actor?.knownFor[indexPath.row].id {
            viewModel.delegate?.viewModelShowMovieDetail(movieId: id)
        } else {
            return
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 155, height: 200)
    }
}
