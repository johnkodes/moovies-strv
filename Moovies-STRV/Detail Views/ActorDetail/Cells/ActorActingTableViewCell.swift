//
//  ActorActingTableViewCell.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 13/09/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import UIKit

class ActorActingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var yeearLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    func setupWithData(data: MovieListItem) {
        
        guard let movie = data as? MovieStub else {
            return
        }
        
        yeearLabel.text = movie.releaseYear?.getYear()
        titleLabel.text = movie.title
    }
    
}
