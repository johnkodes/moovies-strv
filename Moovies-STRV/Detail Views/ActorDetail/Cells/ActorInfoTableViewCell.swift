//
//  ActorInfoTableViewCell.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 13/09/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import UIKit

class ActorInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    
    func setupWithData(label: String, content: String) {
        
        headingLabel.text = label.uppercased()
        headingLabel.addTextSpacing(spacing: 2)
        
        contentLabel.text = content
    }
    
}
