//
//  ActorDetailViewModel.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 13/09/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import Foundation

//MovieStub, MovieListItem

protocol ActorDetailListItem {
    
    var id: String { get }
    var name: String { get }
    var job: [String] { get }
    var biography: String { get }
    var birthdate: String { get }
    var placeOfBirth: String { get }
    var image: ImageReference? { get }
    
    var knownFor: [MovieListItem] { get }
}

protocol ActorMovieTableListItem {
    
    var id: String { get }
    var title: String { get }
    var character: String { get }
}

struct ActorMovieTableItem: ActorMovieTableListItem {
    
    var id: String
    var title: String
    var character: String
}

struct ActorDetailItem: ActorDetailListItem {
    
    var id: String
    var name: String
    var job: [String]
    var biography: String
    var birthdate: String
    var placeOfBirth: String
    var image: ImageReference?
    
    var knownFor: [MovieListItem]
    var acting: [MovieStub]
}

protocol ActorDetailViewModelDelegate: class {
    func viewModelItemUpdated(item: ActorDetailItem)
    func viewModelChangedState(state: ActorDetailViewModel.State)
    func viewModelShowMovieDetail(movieId: String)
}

enum ActorDetailTableViewSectionType: String {
    
    case birthday = "Birthday"
    case placeOfBirth = "Place of Birth"
    case knownFor = "Known For"
    case acting = "Acting"
}

class ActorDetailViewModel {
    
    enum State {
        case empty
        case loading
        case ready
        case error
    }
    
    var error: Error?
    
    var state: State = .empty {
        didSet {
            if state != oldValue {
                delegate?.viewModelChangedState(state: state)
            }
        }
    }
    
    weak var delegate: ActorDetailViewModelDelegate?
    let actorSource: ActorSource
    
    var actor: ActorDetailItem?
    var actorId: String = "1"
    
    func reloadActor() {
        if state == .loading {
            return
        }
        
        state = .loading
        
        actorSource.fetchActorDetail(id: actorId) { result in
            if let actorItem = result.value {
                
                self.actor = ActorDetailItem(
                    id: actorItem.id,
                    name: actorItem.name,
                    job: ["Actor"],
                    biography: actorItem.biography,
                    birthdate: actorItem.birthday?.dateToString() ?? "",
                    placeOfBirth: actorItem.placeOfBirth ?? "",
                    image: actorItem.profileImage,
                    knownFor: actorItem.cast.sorted { $0.popularity > $1.popularity }.map {
                        MovieStub(
                            id: $0.id,
                            title: $0.title,
                            image: $0.image,
                            popularity: $0.popularity,
                            score: $0.score,
                            releaseYear: $0.releaseDate
                        )
                    },
                    acting: actorItem.cast.sorted {
                            guard
                                let date1 = $0.releaseDate,
                                let date2 = $1.releaseDate
                            else {
                                    return false
                            }
                            return date1.timeIntervalSince1970 > date2.timeIntervalSince1970
                        
                        }.map {
                        MovieStub(
                            id: $0.id,
                            title: $0.title,
                            image: $0.image,
                            popularity: $0.popularity,
                            score: $0.score,
                            releaseYear: $0.releaseDate
                        )
                    }
                )
                
            
                self.state = self.actor != nil ? .empty : .ready
                
                if let actor = self.actor {
                    
                    self.delegate?.viewModelItemUpdated(item: actor)
                }
                
                
            } else {
                self.state = .error
                self.error = result.error
            }
        }
    }
    
    init(actorSource: ActorSource = AlamofireActorSource()) {
        self.actorSource = actorSource
    }
    

}
