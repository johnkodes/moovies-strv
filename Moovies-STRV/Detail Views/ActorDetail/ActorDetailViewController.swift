//
//  ActorDetailViewController.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 13/09/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import UIKit
import AlamofireImage

class ActorDetailViewController: UIViewController, StoryboardInit {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var actorImage: UIImageView!
    @IBOutlet weak var actorJob: UILabel!
    @IBOutlet weak var actorName: UILabel!
    @IBOutlet weak var actorBiography: UILabel!
    
    var viewModel: ActorDetailViewModel!
    
    let sections: [ActorDetailTableViewSectionType] = [
        .birthday,
        .placeOfBirth,
        .knownFor,
        .acting
    ]
    
    var actorId: String?
    
    var actor: ActorDetailItem? {
        
        didSet {
            guard let actor = actor else { return }
            
            let filter = AspectScaledToFillSizeFilter(
                size: actorImage.frame.size
            )
            
            if let image = actor.image {
                actorImage.af_setImage(
                    withURL: image.url(width: actorImage.bounds.width),
                    placeholderImage: #imageLiteral(resourceName: "placeholder-movie"),
                    filter: filter,
                    imageTransition: .crossDissolve(0.2),
                    completion: nil)
                
            } else {
                actorImage.image = #imageLiteral(resourceName: "placeholderActor")
                let overlay = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
                overlay.frame = actorImage.frame
            
                actorImage.addSubview(overlay)
                actorImage.setupImageOverlay()
            }
        
            actorJob.text = actor.job.joined(separator: ", ").uppercased()
            actorJob.addTextSpacing(spacing: 2)
            
            actorName.text = actor.name
            
            let biography = actor.biography.prefix(230)
            
            actorBiography.text = String(biography.appending("..."))
            actorBiography.setLineHeight(lineHeight: 5)
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        // UI
        setupUI()
        
        viewModel = ActorDetailViewModel()
        viewModel.delegate = self
        
        guard
            let actorId = actorId
        else {
                viewModel.state = .error
                return
        }
        
        viewModel.actorId = actorId
        viewModel.reloadActor()
    }
}

extension ActorDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if sections[indexPath.section] == .acting {
            
            guard let actor = viewModel.actor else {
                return
            }
            
            viewModelShowMovieDetail(movieId: actor.acting[indexPath.row].id)
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let actor = actor else {
            print("no actor was found")
            return UITableViewCell()
        }
        
        switch sections[indexPath.section] {
        case .birthday:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ActorInfoTableViewCell") as? ActorInfoTableViewCell else {
                
                print("Creating empty table view cell")
                return UITableViewCell()
            }
            
            cell.setupWithData(label: sections[indexPath.section].rawValue, content: actor.birthdate == "" ? "No birthdate info." : actor.birthdate)
            return cell
            
        case .placeOfBirth:
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ActorInfoTableViewCell") as? ActorInfoTableViewCell else {
                
                print("Creating empty table view cell")
                return UITableViewCell()
            }
            
            cell.setupWithData(label: sections[indexPath.section].rawValue, content: actor.placeOfBirth)
            return cell
            
        case .knownFor:
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ActorDetailMovieTableViewCell") as? ActorDetailMovieTableViewCell else {
                
                print("Creating empty table view cell")
                return UITableViewCell()
            }
            
            cell.collectionView.tag = indexPath.section
            cell.viewModel = viewModel
            
            cell.collectionView.reloadData()
            
            return cell
            
        case .acting:
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ActorActingTableViewCell") as? ActorActingTableViewCell else {
                
                print("Creating empty table view cell")
                return UITableViewCell()
            }
            
            cell.setupWithData(data: actor.acting[indexPath.row])
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if sections[section] == .acting {
            guard let actor = actor else {
                return 0
            }

            return actor.acting.count > 10 ? 10 : actor.acting.count
            
        } else {
            return 1
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch sections[indexPath.section] {
        case .birthday, .placeOfBirth, .acting:
            return UITableViewAutomaticDimension
        case .knownFor:
            return 200
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        guard let headerCell = tableView.dequeueReusableCell(withIdentifier: "DetailSectionHeaderTableViewCell") as? DetailSectionHeaderTableViewCell else {
            print("DetailSectionHeaderTableViewCell not found")
            return UITableViewCell()
        }
        
        if sections[section] == .placeOfBirth || sections[section] == .birthday {
            //headerCell.setupWithData(label: "")
            //return headerCell
            return nil
        } else {
            headerCell.setupWithData(label: sections[section].rawValue)
            return headerCell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch sections[section] {
        case .knownFor, .acting:
            return 50
        default:
            return CGFloat.leastNormalMagnitude
        }
    }
}

extension ActorDetailViewController: ActorDetailViewModelDelegate {
    
    func viewModelShowMovieDetail(movieId: String) {
        
        let moviesDetailViewController = MovieDetailViewController.storyboardInit()
        moviesDetailViewController.movieId = movieId
        
        self.navigationController?.pushViewController(moviesDetailViewController, animated: true)
    }
    
    func viewModelItemUpdated(item: ActorDetailItem) {
        
        actor = item
        tableView.reloadData()
    }
    
    func viewModelChangedState(state: ActorDetailViewModel.State) {
        
        viewModel.state = state
        
        let overlayView = UIScreen.main.snapshotView(afterScreenUpdates: false)
        overlayView.tag = 88
        let overlay = UIVisualEffectView()
        
        switch state {
            
        case .loading:
            
            overlayView.frame = self.view.frame
            overlay.frame = overlayView.frame
            overlay.effect = UIBlurEffect(style: .dark)
            
            self.view.addSubview(overlayView)
            overlayView.addSubview(overlay)
            
        case .ready, .empty:
            removeOverlay()
            
        case .error:
            //removeOverlay()
            
            let alertPresenter = AlertPresenter()
            let alert = UIAlertController(title: "Error", message: viewModel.error?.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .default) {_ in
            })
            
            alert.addAction(UIAlertAction(title: "Try again", style: .default) {_ in
                self.viewModel.reloadActor()
            })
            
            alertPresenter.presentAlertController(controller: alert, fromController: self)
        }
        
    }
    
    func removeOverlay() {
        guard let view = self.view.viewWithTag(88) else {
            return
        }
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .transitionCrossDissolve, animations: {
            view.alpha = 0
        }, completion: { finished in
            view.removeFromSuperview()
        })
    }
    
}

extension ActorDetailViewController {
    
    func setupUI() {
        navigationController?.presentTransparentNavigationBar()
        actorImage.setupImageOverlay()
        
        var nib = UINib(nibName: "ActorInfoTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "ActorInfoTableViewCell")
        
        nib = UINib(nibName: "ActorActingTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "ActorActingTableViewCell")
        
        nib = UINib(nibName: "ActorDetailMovieTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "ActorDetailMovieTableViewCell")
        
        nib = UINib(nibName: "DetailSectionHeaderTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "DetailSectionHeaderTableViewCell")
    }
}
