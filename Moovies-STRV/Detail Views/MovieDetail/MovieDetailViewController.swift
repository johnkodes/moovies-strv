//
//  MovieDetailViewController.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 02/08/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import UIKit
import AlamofireImage

class MovieDetailViewController: UIViewController, StoryboardInit {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var movieCategory: UILabel!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var moviePlot: UILabel!
    @IBOutlet weak var movieCreators: UILabel!
    @IBOutlet weak var ratingView: CircleRatingView!
    
    var viewModel: MovieDetailViewModel!
    
    // Table View Cell Types
    var sections: [MovieDetailTableViewSectionType] = []
    
    var movieId: String = "550"
    
    var movieItem: MovieItem? {
        
        didSet {
            guard let movie = movieItem else { return }
            
            let filter = AspectScaledToFillSizeFilter(
                size: movieImage.frame.size
            )
            
            if let image = movie.image {
                
                movieImage.af_setImage(
                    withURL: image.url(width: movieImage.frame.width),
                    placeholderImage: #imageLiteral(resourceName: "placeholder-movie"),
                    filter: filter,
                    imageTransition: .crossDissolve(0.2),
                    completion: nil)
            } else {
                
                movieImage.image = #imageLiteral(resourceName: "placeholderMovie2")
                let overlay = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
                overlay.frame = movieImage.frame
                
                movieImage.addSubview(overlay)
                movieImage.setupImageOverlay()
            }
            
            if let score = Double(movie.score) {
                ratingView.percentage = CGFloat(score)
                ratingView.showRating(animated: true)
            }
            
            movieCategory.text = movie.genres.joined(separator: ", ").uppercased()
            movieCategory.addTextSpacing(spacing: 2)
            
            movieCreators.text = movie.creators.joined(separator: ", ")
            movieTitle.text = movie.title
            moviePlot.text = String(movie.overview.prefix(200).appending("..."))
            moviePlot.setLineHeight(lineHeight: 7)
            
            if movie.stars.count > 0 { sections.append(.stars) }
            if movie.trailers.count > 0 { sections.append(.trailers) }
            if movie.gallery.count > 0 { sections.append(.gallery) }
            if movie.reviews.count > 0 { sections.append(.reviews) }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        viewModel = MovieDetailViewModel()
        
        viewModel.movieId = movieId
        viewModel.delegate = self
        
        viewModel.reloadMovie()
    }
    
}

extension MovieDetailViewController: MovieDetailViewModelDelegate {
    
    func viewModelShowActorDetail(id: String) {
        
        let actorDetailViewController = ActorDetailViewController.storyboardInit()
        actorDetailViewController.actorId = id
        self.navigationController?.pushViewController(actorDetailViewController, animated: true)
    }
    
    func viewModelItemUpdated(item: MovieItem) {
        
        movieItem = item
        tableView.reloadData()
    }
    
    func viewModelChangedState(state: MovieDetailViewModel.State) {
        
        let overlayView = UIScreen.main.snapshotView(afterScreenUpdates: false)
        overlayView.tag = 88
        let overlay = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
        
        switch state {
            
        case .loading:
            
            overlayView.frame = self.view.frame
             overlay.frame = overlayView.frame
            
            self.view.addSubview(overlayView)
            overlayView.addSubview(overlay)
        
        case .ready, .empty:
            removeOverlay()

        case .error:
            //removeOverlay()
            
            let alertPresenter = AlertPresenter()
            let alert = UIAlertController(title: "Error", message: viewModel.error?.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .default) {_ in
                //self.dismiss(animated: true, completion: nil)
            })
            
            alert.addAction(UIAlertAction(title: "Try again", style: .default) {_ in
                //self.dismiss(animated: true, completion: nil)
                self.viewModel.reloadMovie()
            })
            
            alertPresenter.presentAlertController(controller: alert, fromController: self)
        }
    }
    
    func removeOverlay() {
        guard let view = self.view.viewWithTag(88) else {
            return
        }
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .transitionCrossDissolve, animations: {
            view.alpha = 0
        }, completion: { finished in
            view.removeFromSuperview()
        })
    }
    
    func viewModelShowMedia(id: String) {
        let mediaController = MediaViewController()
        mediaController.id = id
        mediaController.type = .video
        mediaController.modalPresentationStyle = .custom
        mediaController.modalTransitionStyle = .crossDissolve
        
        self.present(mediaController, animated: true, completion: nil)
    }
    
    func viewModelShowMedia(image: ImageReference?) {
        let mediaController = MediaViewController()
        mediaController.url = image?.url(width: 999)
        mediaController.type = .image
        
        self.present(mediaController, animated: true, completion: nil)
    }
    
    
}

extension MovieDetailViewController: UITableViewDataSource, UITableViewDelegate {
    
    // MARK: Table View Cells
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch sections[indexPath.section] {
            
        case .gallery, .stars, .trailers:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "MovieDetailTableCell") as? MovieDetailTableViewCell else {
            
            print("Creating empty table view cell")
            return UITableViewCell()
            }
            
            cell.collectionView.tag = indexPath.section
            cell.cellType = sections[indexPath.section]
            cell.viewModel = viewModel
            
            cell.collectionView.reloadData()
            
            return cell
            
        case .reviews:
            
            guard
                let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewTableViewCell") as? ReviewTableViewCell,
                let review = movieItem?.reviews[indexPath.row]
            else {
                
                print("No Reviews, Creating empty table view cell")
                return UITableViewCell()
            }
            
            if indexPath.row == (tableView.numberOfRows(inSection: indexPath.section)-1) {
                cell.borderView.isHidden = true
            }
            
            cell.setupWithData(data: review)
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if sections[section] == .reviews {
            return movieItem?.reviews.count ?? 0
        } else {
            return 1
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        //return viewModel.data.count
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch sections[indexPath.section] {
        case .reviews:
            return UITableViewAutomaticDimension
        case .stars:
            return 220
        case .gallery, .trailers:
            return 170
        }
    }
    
    // MARK: Table View Section Header
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        //put into viewModel
        
        guard let headerCell = tableView.dequeueReusableCell(withIdentifier: "DetailSectionHeaderTableViewCell") as? DetailSectionHeaderTableViewCell else {
            print("DetailSectionHeaderTableViewCell not found")
            return UITableViewCell()
        }
        
        headerCell.setupWithData(label: sections[section].rawValue)
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
}

extension MovieDetailViewController {
    
    func setupUI() {
        // TableView Setup
        var nib = UINib(nibName: "MovieDetailTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "MovieDetailTableCell")
        
        nib = UINib(nibName: "DetailSectionHeaderTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "DetailSectionHeaderTableViewCell")
        
        nib = UINib(nibName: "ReviewTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "ReviewTableViewCell")
        
        tableView.dataSource = self
        tableView.delegate = self
        
        // UI
        navigationController?.presentTransparentNavigationBar()
        movieImage.setupImageOverlay()
    }
}
