//
//  ReviewTableViewCell.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 19/09/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import UIKit

class ReviewTableViewCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textView: UILabel!
    @IBOutlet weak var borderView: UIView!
    
    func setupWithData(data: ReviewListItem) {
        
        label.text = data.author
        textView.text = data.content
    }
    
}
