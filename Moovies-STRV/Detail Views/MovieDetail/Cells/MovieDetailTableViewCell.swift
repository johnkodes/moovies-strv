//
//  MovieDetailTableViewCell.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 08/08/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import UIKit

class MovieDetailTableViewCell: UITableViewCell {
   
    @IBOutlet weak var collectionView: UICollectionView!
    
    var cellType: MovieDetailTableViewSectionType = MovieDetailTableViewSectionType.gallery
    //var movie: MovieItem
    
    var viewModel: MovieDetailViewModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        var nib = UINib(nibName: "GalleryCollectionViewCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "GalleryCollectionViewCell")
        
        nib = UINib(nibName: "PersonCollectionViewCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "PersonCollectionCell")
        
        nib = UINib(nibName: "PersonCollectionViewCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "PersonCollectionCell")
        
        nib = UINib(nibName: "TrailerCollectionViewCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "TrailerCollectionViewCell")
        
    }
    
}

extension MovieDetailTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard
            let movie = viewModel.movie
        else {
            print("No movie exists")
            return
        }
        
        switch cellType {
            
        case .trailers:
            viewModel.delegate?.viewModelShowMedia(id: movie.trailers[indexPath.row].key)
        case .gallery:
            viewModel.delegate?.viewModelShowMedia(image: movie.gallery[indexPath.row].image)
        case .stars:

            if let id = viewModel.movie?.stars[indexPath.row].id {
                viewModel.delegate?.viewModelShowActorDetail(id: id)
            }
            
        case .reviews:
            return
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        guard let movie = viewModel.movie else {
            print("No movie found")
            return 0
        }
        
        switch cellType {
            
        case .gallery: return movie.gallery.count
        case .stars: return movie.stars.count
        case .trailers: return movie.trailers.count
        case .reviews: return movie.reviews.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        /*guard
            let section = viewModel.getSection(index: collectionView.tag),
            let dataItem = section.getDataItem(index: indexPath.row)
            else {
                
                return UICollectionViewCell()
        } */
        
        
        switch cellType {
            
        case .gallery:

            
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCollectionViewCell", for: indexPath) as? GalleryCollectionViewCell else {
                
                print("Creating empty collection view cell")
                return UICollectionViewCell()
            }
            
            if let gallery = viewModel.movie?.gallery[indexPath.row] {
                cell.setupWithData(data: gallery)
            }
            
            return cell
            
        case .reviews:
            
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PersonCollectionCell", for: indexPath) as? PersonCollectionViewCell else {
                
                print("Creating empty collection view cell")
                return UICollectionViewCell()
            }
            
            //cell.setupWithData(data: dataItem)
            return cell
            
        case .stars:
            
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PersonCollectionCell", for: indexPath) as? PersonCollectionViewCell else {
                
                print("Creating empty collection view cell")
                return UICollectionViewCell()
            }
            
            if let actor = viewModel.movie?.stars[indexPath.row] {
                cell.setupWithData(data: actor)
            }

            return cell
            
        case .trailers:
            
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrailerCollectionViewCell", for: indexPath) as? TrailerCollectionViewCell else {
                
                print("Creating empty collection view cell")
                return UICollectionViewCell()
            }
            
            if let trailer = viewModel.movie?.trailers[indexPath.row] {
                cell.setupWithData(data: trailer)
            }
            
            return cell
        }

    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var width: Int = 155
        var height: Int = 200

        switch cellType {
        case .gallery:
            width = 155
            height = 152
        case .stars:
            width = 155
            height = 270
        case .trailers:
            width = 326
            height = 152
        case .reviews:
            width = 326
            height = 200
            
        }
        
        return CGSize(width: width, height: height)
    }
}
