//
//  MovieDetailHeaderTableViewCell.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 08/08/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import UIKit

class DetailSectionHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var showAll: UIButton!
    @IBOutlet weak var label: UILabel!
    
    func setupWithData(label: String) {
        
        self.label.text = label
        showAll.isHidden = true
    }
}
