//
//  MovieDetailViewModel.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 08/08/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import Foundation

enum MovieDetailTableViewSectionType: String {
    
    case stars = "Stars"
    case gallery = "Gallery"
    case trailers = "Trailers"
    case reviews = "Reviews"
}

struct MovieDetailTableViewSection {
    
    let id: Int
    let type: MovieDetailTableViewSectionType
}

protocol MovieDetailItem {
    var id: String { get }
    var title: String { get }
    var genres: [String] { get }
    var overview: String { get }
    var score: String { get }
    var creators: [String] { get }
    var image: ImageReference? { get }
    
    var stars: [ActorListItem] { get }
    var trailers: [TrailerListItem] { get }
    var gallery: [GalleryListItem] { get }
    var reviews: [ReviewListItem] { get }
}


// MARK: Protocols

protocol GalleryListItem {
    var id: String { get }
    var image: ImageReference? { get }
}

protocol ReviewListItem {
    var id: String { get }
    var author: String { get }
    var content: String { get }
}

protocol TrailerListItem {
    var id: String { get }
    var name: String { get }
    var key: String { get }
    var type: String { get }
    var site: String { get }
    
    var imageURL: URL { get }
}

protocol ActorListItem {
    var id: String { get }
    var name: String { get }
    var character: String? { get }
    var movies: [String]? { get }
    var image: ImageReference? { get }
}

struct GalleryItem: GalleryListItem {
    var id: String
    var image: ImageReference?
}

struct ReviewItem: ReviewListItem {
    var id: String
    var author: String
    var content: String
}

struct TrailerItem: TrailerListItem {
    var id: String
    var key: String
    var name: String
    var type: String
    var site: String
    
    var imageURL: URL { return URL(string: "http://img.youtube.com/vi/\(key)/mqdefault.jpg")! }
}

struct ActorItem: ActorListItem {
    var id: String
    var name: String
    var character: String?
    var movies: [String]?
    var image: ImageReference?
}

struct MovieItem: MovieDetailItem {
    var id: String
    var title: String
    var genres: [String]
    var overview: String
    var score: String
    var creators: [String]
    var image: ImageReference?
    
    var stars: [ActorListItem]
    var trailers: [TrailerListItem]
    var gallery: [GalleryListItem]
    var reviews: [ReviewListItem]
}

protocol MovieDetailViewModelDelegate: class {
    func viewModelItemUpdated(item: MovieItem)
    func viewModelChangedState(state: MovieDetailViewModel.State)
    func viewModelShowActorDetail(id: String)
    func viewModelShowMedia(id: String)
    func viewModelShowMedia(image: ImageReference?)
}

class MovieDetailViewModel {
    
    enum State {
        case empty
        case loading
        case ready
        case error
    }
    
    var error: Error?
    
    var state: State = .empty {
        didSet {
            if state != oldValue {
                delegate?.viewModelChangedState(state: state)
            }
        }
    }
    
    weak var delegate: MovieDetailViewModelDelegate?
    let movieSource: MovieSource
    
    var movie: MovieItem?
    
    var movieId: String = ""
    
    func reloadMovie() {
        if state == .loading {
            return
        }
        
        state = .loading
        
        movieSource.fetchMovieDetail(id: movieId) { result in
            
            if let movieItem = result.value {
                
                let movieImage: ImageReference
                
                if let image = movieItem.movieImage {
                    movieImage = image
                } else {
                    movieImage = movieItem.posterImage
                }
                
                self.movie = MovieItem(
                                id: String(movieItem.id),
                                title: movieItem.title,
                                genres: movieItem.genres.map { $0.name },
                                overview: movieItem.overview,
                                score: String(movieItem.score),
                                creators: movieItem.crew.filter { $0.job == "Director" }.map { return $0.name ?? "" },
                                image: movieImage,
                                stars: movieItem.cast.map {
                                            ActorItem(
                                                id: $0.actorId,
                                                name: $0.name ?? "No name",
                                                character: $0.character,
                                                movies: nil,
                                                image: $0.profileImage)
                                },
                                trailers: movieItem.videos.map {
                                    TrailerItem(id: $0.id, key: $0.key, name: $0.name, type: $0.type.rawValue, site: $0.site.rawValue)
                                },
                                gallery: movieItem.images.map {
                                    GalleryItem(id: "1", image: $0.image)
                                },
                                reviews: movieItem.reviews.map {
                                    ReviewItem(id: $0.id, author: $0.author, content: $0.content)
                                })
                
                
                self.state = self.movie != nil ? .empty : .ready
                
                if let movie = self.movie {
                    self.delegate?.viewModelItemUpdated(item: movie)
                }
                
            } else {
                self.state = .error
                self.error = result.error
            }
        }
    }
    
    init(movieSource: MovieSource = AlamofireMovieSource()) {
        self.movieSource = movieSource
    }
    
}




