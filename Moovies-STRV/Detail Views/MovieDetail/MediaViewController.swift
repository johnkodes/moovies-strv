//
//  YoutubeViewController.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 19/09/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import UIKit
import AlamofireImage

class MediaViewController: UIViewController, UIScrollViewDelegate {
    
    enum MediaType {
        case image
        case video
    }

    var imgView: UIImageView!
    var type: MediaType = .video
    var id = ""
    var url: URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch type {
        case .video:
            playVideo(id: id)
        case .image:
            guard let url = url else {
                return
            }
            showImage(url: url)
        }
        
        let closeButton = UIButton()
        closeButton.frame = CGRect(x: self.view.frame.width - 50, y: 24, width: 30, height: 30)
        
        if type == .image {
            closeButton.backgroundColor = UIColor.init(named: "dark")
        }
        
        closeButton.setImage(#imageLiteral(resourceName: "icCancel"), for: .normal)
        closeButton.addTarget(self, action: #selector(MediaViewController.closeAction), for: .touchUpInside)
        self.view.addSubview(closeButton)
            }
    
    @objc func closeAction() {
        self.dismiss(animated: true) { () -> Void in
            UIDevice.current.setValue(Int(UIInterfaceOrientation.portrait.rawValue), forKey: "orientation")
        }
    }
    
    func playVideo(id: String){
        let width = self.view.frame.width
        let height: CGFloat = 300.0
        
        self.view.backgroundColor = UIColor.init(named: "dark")
        
        let web = UIWebView(frame: CGRect(x: 0, y: self.view.frame.size.height / 2 - height / 2, width: width, height: height))
        web.backgroundColor = UIColor.init(named: "dark")
        web.isOpaque = false
        self.view.addSubview(web)
        web.mediaPlaybackRequiresUserAction = false
        web.allowsInlineMediaPlayback = true
        let videoHtml = "<html><head><style>body{margin:0px 0px 0px 0px; background: transparent; }</style></head> <body> <div id=\"player\"></div> <script> var tag = document.createElement('script'); tag.src = 'http://www.youtube.com/player_api'; var firstScriptTag = document.getElementsByTagName('script')[0]; firstScriptTag.parentNode.insertBefore(tag, firstScriptTag); var player; function onYouTubePlayerAPIReady() { player = new YT.Player('player', { width:'\(width)', height:'\(height)', videoId:'\(id)', events: { 'onReady': onPlayerReady }, playerVars: { controls: 0, rel: 0, modestbranding: 1, html5:1, showinfo:0, playsinline: 1 } }); } function onPlayerReady(event) { event.target.playVideo(); } </script> </body> </html>";
        web.loadHTMLString(videoHtml, baseURL: Bundle.main.resourceURL)
    }
    
    func showImage(url: URL) {
        let scrollImg: UIScrollView = UIScrollView()
        scrollImg.delegate = self
        scrollImg.frame = self.view.frame
        scrollImg.backgroundColor = UIColor.init(named: "dark")
        scrollImg.alwaysBounceVertical = false
        scrollImg.alwaysBounceHorizontal = false
        scrollImg.showsVerticalScrollIndicator = true
        scrollImg.flashScrollIndicators()
        scrollImg.minimumZoomScale = 1.0
        scrollImg.maximumZoomScale = 6.0
        scrollImg.contentInsetAdjustmentBehavior = .automatic
        
        let progressView: UIProgressView = UIProgressView(frame: CGRect(x: 20, y: self.view.frame.height / 2, width: self.view.frame.width - 40, height: 20))
        progressView.progressTintColor = UIColor.init(named: "red")
        progressView.trackTintColor = UIColor.init(named: "white30")
        progressView.setProgress(0, animated: false)
        progressView.isHidden = true

        scrollImg.addSubview(progressView)
        
        imgView = UIImageView()
        imgView.frame = scrollImg.frame
        
        let filter = AspectScaledToFillSizeFilter(
            size: imgView.frame.size
        )
        
        imgView.af_setImage(
            withURL: url,
            //placeholderImage: #imageLiteral(resourceName: "placeholder-movie"),
            filter: filter,
            progress: { p in
                if progressView.isHidden && p.fractionCompleted == 1 {
                    return
                }
                
                progressView.isHidden = false
                progressView.setProgress(Float(p.fractionCompleted), animated: true)
            },
            imageTransition: .crossDissolve(0.3),
            completion: { response in
                progressView.isHidden = true
            })
        
        self.view.addSubview(scrollImg)
        scrollImg.addSubview(imgView)
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imgView
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        
    }
}
