//
//  APIClient.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 22/08/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import Foundation
import Alamofire
import UnboxedAlamofire

struct APIClient {
    
    func request(withRouter url: URLRequestConvertible, _ completionHandler: @escaping (Any?, Error?) -> Void) {
        
        Alamofire.request(url).validate().responseJSON { (response) in
            
            switch response.result {
            case .success(let data):
                completionHandler(data, nil)
                
            case .failure(let error):
                completionHandler(nil, error)
            }
            
            //let movies = unbox atKey
        }
        
    }

}
