//
//  Configuration.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 22/08/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import Foundation
import Unbox

struct Configuration: Unboxable {
    
    let baseUrl: URL
    let secureBaseUrl: URL
    
    init(unboxer: Unboxer) throws {
        baseUrl = try unboxer.unbox(key: "base_url")
        secureBaseUrl = try unboxer.unbox(key: "secure_base_url")
    }
}
