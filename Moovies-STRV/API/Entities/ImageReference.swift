//
//  ImageReference.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 21/08/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import Foundation
import Unbox
import ReachabilitySwift

struct ImageReference: UnboxableByTransform {

    typealias UnboxRawValue = String
    
    let imagePath: String
    let reachability = Reachability()!
    let pixelDensity = Settings.pixelDensity
    
    static func transform(unboxedValue: String) -> ImageReference? {
        return ImageReference(imagePath: unboxedValue)
    }
    
    // Load the correct images url from Settings?
    static var baseImageURL: URL {
        return URL(string:"https://image.tmdb.org/t/p/")!
    }
    
    func url(width: CGFloat) -> URL {
        var size: Sizes = .original
        
        let width = Int(width) * pixelDensity
        
        switch width {
        case 0..<92:
            size = .w92
        case 92..<154:
            size = .w154
        case 154..<185:
            size = .w185
        case 185..<342:
            size = .w342
        case 342..<500:
            size = .w500
        case 500..<780:
            size = .w780
        default:
            size = .original
        }
        
        return ImageReference.baseImageURL.appendingPathComponent(size.rawValue).appendingPathComponent(imagePath)
    }
    
    var fullURL: URL {
        
        // add usable sizes
        return ImageReference.baseImageURL.appendingPathComponent("w342").appendingPathComponent(imagePath)
    }
    
    enum Sizes: String {
        case w92 = "w92"
        case w154 = "w154"
        case w185 = "w185"
        case w342 = "w342"
        case w500 = "w500"
        case w780 = "w780"
        case original = "original"
    }
}
