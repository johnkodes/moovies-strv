//
//  Account.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 21/08/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import Foundation
import Unbox

struct APIAccount: Unboxable {
    
    let id: String
    let avatar: APIGravatar
    let name: String
    let username: String
    
    init(unboxer: Unboxer) throws {
        
        id = try unboxer.unbox(key: "id")
        avatar = try unboxer.unbox(keyPath: "avatar.gravatar.hash")
        name = try unboxer.unbox(key: "name")
        username = try unboxer.unbox(key: "username")
    }
}

struct APIGravatar: UnboxableByTransform {
    
    typealias UnboxRawValue = String
    
    let imageHash: String
    
    static func transform(unboxedValue: String) -> APIGravatar? {
        return APIGravatar(imageHash: unboxedValue)
    }
    
    // Load the correct images url from Settings?
    static var baseImageURL: URL {
        return URL(string: Settings.gravatarBaseUrl)!
    }
    
    var fullURL: URL {
        // add usable sizes
        return APIGravatar.baseImageURL.appendingPathComponent(imageHash)
    }
}

