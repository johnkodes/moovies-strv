//
//  Person.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 21/08/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import Foundation
import Unbox

struct APIActorFull: Unboxable {
    
    let id: String
    let name: String
    let cast: [APIMovieStub]
    let biography: String
    let profileImage: ImageReference?
    let placeOfBirth: String?
    let birthday: Date?
    let deathday: Date?
    
    init(unboxer: Unboxer) throws {
        let df = DateFormatter()
        df.dateFormat = "YYYY-MM-dd"
        
        id = try unboxer.unbox(key: "id")
        name = try unboxer.unbox(key: "name")
        cast = try unboxer.unbox(keyPath: "movie_credits.cast")
        profileImage = unboxer.unbox(key: "profile_path")
        biography = try unboxer.unbox(key: "biography")
        birthday = unboxer.unbox(key: "birthday", formatter: df)
        deathday = unboxer.unbox(key: "deathday", formatter: df)
        placeOfBirth = unboxer.unbox(key: "place_of_birth")
    }
}

struct APIActorStub: Unboxable {
    
    let id: String
    let profileImage: ImageReference?
    let name: String
    let knownFor: [APIActorKnownFor]
    
    init(unboxer: Unboxer) throws {
        
        id = try unboxer.unbox(key: "id")
        profileImage = unboxer.unbox(key: "profile_path")
        name = try unboxer.unbox(key: "name")
        knownFor = try unboxer.unbox(key: "known_for")
    }
}

struct APIActorResults: Unboxable {
    let results: [APIActorStub]
    
    init(unboxer: Unboxer) throws {
        results = try unboxer.unbox(key: "results")
    }
}


struct APIMovieActor: Unboxable {
    
    enum ActorGender: Int, UnboxableEnum {
        case male = 1
        case female = 2
    }
   
    let character: String?
    let name: String?
    let profileImage: ImageReference?
    let order: Int?
    let gender: ActorGender?
    let actorId: String
    
    init(unboxer: Unboxer) throws {
        
        character = unboxer.unbox(key: "character")
        name = unboxer.unbox(key: "name")
        profileImage = unboxer.unbox(key: "profile_path")
        order = unboxer.unbox(key: "order")
        actorId = try unboxer.unbox(key: "id")
        gender = unboxer.unbox(key: "gender")
    }
}

struct APIActorKnownFor: Unboxable {
    
    let id: String
    let title: String?
    let name: String?
    let poster: ImageReference?
    
    init(unboxer: Unboxer) throws {
        id = try unboxer.unbox(key: "id")
        title = unboxer.unbox(key: "title")
        name = unboxer.unbox(key: "name")
        poster = unboxer.unbox(key: "poster_path")
    }
}

struct APIMovieCrew: Unboxable {
    
    let department: String
    let name: String?
    let profileImage: ImageReference?
    let job: String
    
    init(unboxer: Unboxer) throws {
        
        department = try unboxer.unbox(key: "department")
        name = unboxer.unbox(key: "name")
        profileImage = unboxer.unbox(key: "profile_path")
        job = try unboxer.unbox(key: "job")
    }
    
}
