//
//  Authentication.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 19/09/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import Foundation
import Unbox

struct APIToken: Unboxable {
    
    let expiresAt: String
    let success: Bool
    let requestToken: String
    
    init(unboxer: Unboxer) throws {
        
        expiresAt = try unboxer.unbox(key: "expires_at")
        success = try unboxer.unbox(key: "success")
        requestToken = try unboxer.unbox(key: "request_token")
    }
}

struct APISession: Unboxable {
    
    let success: Bool
    let sessionId: String
    
    init(unboxer: Unboxer) throws {
        
        success = try unboxer.unbox(key: "success")
        sessionId = try unboxer.unbox(key: "session_id")
    }
}
