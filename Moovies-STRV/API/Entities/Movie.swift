//
//  Movie.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 21/08/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import Foundation
import Unbox

struct APIMovieFull: Unboxable {
    
    let id: Int
    let title: String
    let movieImage: ImageReference?
    let posterImage: ImageReference
    let overview: String
    let releaseDate: Date
    let score: Float
    
    let genres: [APIMovieGenre]
    let videos: [APIMovieVideo]
    
    let images: [APIMovieImage]
    let reviews: [APIReview]
    let cast: [APIMovieActor]
    let crew: [APIMovieCrew]
    
    init(unboxer: Unboxer) throws {
        
        let df = DateFormatter()
        df.dateFormat = "YYYY-MM-dd"
        
        id = try unboxer.unbox(key: "id")
        title = try unboxer.unbox(key: "title")
        movieImage = unboxer.unbox(key: "backdrop_path")
        posterImage = try unboxer.unbox(key: "poster_path")
        releaseDate = try unboxer.unbox(key: "release_date", formatter: df)
        genres = try unboxer.unbox(key: "genres")
        score = try unboxer.unbox(key: "vote_average")
        videos = try unboxer.unbox(keyPath: "videos.results")
        overview = try unboxer.unbox(key: "overview")
        images = try unboxer.unbox(keyPath: "images.posters")
        reviews = try unboxer.unbox(keyPath: "reviews.results")
        cast = try unboxer.unbox(keyPath: "credits.cast")
        crew = try unboxer.unbox(keyPath: "credits.crew")
    }
    
}

struct APIMovieStub: Unboxable {
    let id: String
    let score: Float
    private let movieTitle: String?
    private let tvName: String?
    let title: String
    let genreIds: [Int]
    let overview: String?
    let popularity: Int
    
    private let movieRelease: Date?
    private let tvFirstAir: Date?
    
    let releaseDate: Date?
    let image: ImageReference?
    
    init(unboxer: Unboxer) throws {
        id = try unboxer.unbox(key: "id")
        movieTitle = unboxer.unbox(key: "title")
        tvName = unboxer.unbox(key: "name")
        score = try unboxer.unbox(key: "vote_average")
        genreIds = try unboxer.unbox(key: "genre_ids")
        overview = unboxer.unbox(key: "overview")
        popularity = try unboxer.unbox(key: "popularity")
        image = unboxer.unbox(key: "poster_path")
        
        if let title = movieTitle {
            self.title = title
        } else {
            self.title = tvName!
        }
        
        let df = DateFormatter()
        df.dateFormat = "YYYY-MM-dd"
        
        movieRelease = unboxer.unbox(key: "release_date", formatter: df)
        tvFirstAir = unboxer.unbox(key: "first_air_date", formatter: df)
        
        if let release = movieRelease {
            self.releaseDate = release
        } else {
            if let tvAir = tvFirstAir {
                 self.releaseDate = tvAir
            } else {
                self.releaseDate = nil
            }
        }
    }
}

struct APIMovieResults: Unboxable {
    let results: [APIMovieStub]
    
    init(unboxer: Unboxer) throws {
        results = try unboxer.unbox(key: "results")
    }
}

struct APIMovieImage: Unboxable {
    let voteAverage: String
    let image: ImageReference
    
    init(unboxer: Unboxer) throws {
        voteAverage = try unboxer.unbox(key: "vote_average")
        image = try unboxer.unbox(key: "file_path")
    }
}

struct APIMovieGenre: Unboxable {
    
    let id: Int
    let name: String
    
    init(unboxer: Unboxer) throws {
        
        id = try unboxer.unbox(key: "id")
        name = try unboxer.unbox(key: "name")
    }
}

struct APIReview: Unboxable {
    
    let id: String
    let author: String
    let content: String
    
    init(unboxer: Unboxer) throws {
        
        id = try unboxer.unbox(key: "id")
        author = try unboxer.unbox(key: "author")
        content = try unboxer.unbox(key: "content")
    }
}

struct APIMovieVideo: Unboxable {
    
    enum VideoType: String, UnboxableEnum {
        case trailer = "Trailer"
        case teaser = "Teaser"
        case clip = "Clip"
        case featurette = "Featurette"
    }
    
    enum VideoSite: String, UnboxableEnum {
        case youtube = "YouTube"
    }
    
    let id: String
    let name: String
    let key: String
    let site: VideoSite
    let type: VideoType
    
    init(unboxer: Unboxer) throws {
        
        id = try unboxer.unbox(key: "id")
        key = try unboxer.unbox(key: "key")
        name = try unboxer.unbox(key: "name")
        site = try unboxer.unbox(key: "site")
        type = try unboxer.unbox(key: "type")
    }
}
