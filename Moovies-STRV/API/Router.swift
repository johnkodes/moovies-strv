//
//  Router.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 22/08/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import Foundation
import Alamofire

protocol RouterRequest: URLRequestConvertible {
    
    var method: HTTPMethod { get }
    var path: String { get }
    var params: [String: String]? { get }
}

extension RouterRequest {
    
    var params: [String: String]? { return nil }
    var method: HTTPMethod { return .get }
    
    var defaultParams: [String: String] {
        return [
            "api_key": Settings.apiKey,
            "language": Settings.apiLanguage
        ]
    }
    
    func asURLRequest() throws -> URLRequest {
        
        let url = try Settings.baseURL.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        var finalParams = defaultParams
        
        if let parameters = params {
            parameters.forEach { finalParams[$0] = $1 }
        }
        
        urlRequest = try URLEncoding.default.encode(urlRequest, with: finalParams)
        
        return urlRequest
    }
}

struct Router {
    
    struct Configuration: RouterRequest {
        
        var path: String { return "configuration" }
    }
    
    struct Authentication {
        
        struct RequestToken: RouterRequest {
        
            var path: String { return "authentication/token/new" }
        }
        
        struct CreateSession: RouterRequest {
            
            let requestToken: String
            
            var path: String { return "authentication/session/new" }
            var params: [String : String]? { return ["request_token": requestToken] }
        }
    }
    
    struct Account {
        
        struct Detail: RouterRequest {
           
            let sessionId: String
            
            var path: String { return "account" }
            var params: [String : String]? { return ["session_id": sessionId] }
        }
        
        struct Favorites: RouterRequest {
            
            let sessionId: String
            let accountId: String
            
            var path: String { return "account/\(accountId)/favorite/movies" }
            var params: [String : String]? { return ["session_id": sessionId] }
        }
        
        struct MarkAsFavorite: RouterRequest {
            
            // Request Body
            // media_type, media_id, favorite: bool
            
            let sessionId: String
            let accountId: String
            
            var headers: String { return "default: application/json;charset=utf-8"}
            
            var method: HTTPMethod { return .post }
            var path: String { return "account/\(accountId)/favorite" }
            var params: [String : String]? { return ["session_id": sessionId] }
        }
    }
    
    struct Movie {
        
        // Usage: Router.Movie.Detail(movieId: "12313", details: "videos, images...")
        struct Detail: RouterRequest {
            
            let movieId: String
            var path: String { return "movie/\(movieId)" }
            
            var details: String { return "videos,images,credits,reviews" }
            
            var params: [String : String]? {
                return [
                    "append_to_response": details,
                    "include_image_language": "en, null"
                ]
                
            }
        }
        
        struct Discover: RouterRequest {
            
            enum DiscoverType {
                case nowPlaying
                case popular
                case featured
            }
            
            let type: DiscoverType
            
            let genres: String
            
            var path: String { return "discover/movie" }
            var params: [String : String]? {
                
                switch type {
                case .nowPlaying:
                    return [
                        "region": Settings.apiRegion,
                        "sort_by": "release_date.desc",
                        "include_adult": "false",
                        "include_video": "false",
                        "page": "1",
                        "vote_average.gte": "5",
                        "primary_release_year": Date().getYear(),
                        "primary_release_date_lte": Date().getDate(),
                        "with_genres": genres
                    ]
                case .popular:
                    return [
                        "region": Settings.apiRegion,
                        "sort_by": "popularity.desc",
                        "include_adult": "false",
                        "include_video": "false",
                        "page": "1",
                        "with_genres": genres
                    ]
                case.featured:
                    return [
                        "region": Settings.apiRegion,
                        "sort_by": "popularity.desc",
                        "include_adult": "false",
                        "include_video": "false",
                        "page": "1",
                        "primary_release_year": Date().getYear(),
                        "with_genres": genres
                    ]
                }
            }
        }
        
        struct Upcoming: RouterRequest {
            
            var path: String { return "movie/upcoming" }
        }
        
        struct Genres: RouterRequest {
            
            var path: String { return "genre/movie/list" }
        }
        
    }
    
    struct Person {
        
        struct Detail: RouterRequest {
            
            let personId: String
            var path: String { return "person/\(personId)" }
            
            var details: String { return "movie_credits" }
            
            var params: [String : String]? {
                return [
                    "append_to_response": details,
                    "include_image_language": "en, null"
                ]
                
            }
        }
        
        struct Popular: RouterRequest {
            
            var path: String { return "person/popular" }
        }
    }
    
    struct Search: RouterRequest {

        enum SearchType {
            case movie
            case person
            case multi
        }
        
        let query: String
        let searchType: SearchType
        
        var path: String {
            switch searchType {
            case .movie:
                return "search/movie"
            case .person:
                return "search/person"
            case .multi:
                return "search/multi"
            }
        }
        
        var params: [String : String]? { return ["query": query] }
    }
}
