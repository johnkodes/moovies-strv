//
//  DebugRequestAdapter.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 25/08/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import Foundation
import Alamofire


/**
 RequestAdapters can modify outgoing requests just before they are sent. This is useful when sending latest authorization headers for example.
 */
class DebugRequestAdapter: RequestAdapter {
    
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        print("DebugRequestAdapter called")
        print("Request: \(urlRequest.httpMethod ?? "UNKNOWN") \(String(describing: urlRequest.url?.absoluteString ?? "nil"))")
        print("Headers: \(urlRequest.allHTTPHeaderFields ?? [:])")
        print("---\n")
        var urlRequest = urlRequest // making request mutable
        urlRequest.setValue("Header from adapter", forHTTPHeaderField: "X-Adapter-Header")
        return urlRequest
    }
}
