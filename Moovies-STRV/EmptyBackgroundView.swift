//
//  EmptyBackgroundView.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 12/09/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import UIKit

class EmptyBackgroundView: UIView {

    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var button: UIButton!

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    func setupView() {
        Bundle.main.loadNibNamed("EmptyBackgroundView", owner: self, options: nil)
        
        contentView.frame = self.frame
        //contentView.autoresizingMask = [.flexibleHeight, .flexibleHeight]
        
        addSubview(contentView)
    }

}
