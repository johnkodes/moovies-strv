//
//  SearchTableViewCell.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 01/08/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import UIKit
import AlamofireImage

class SearchTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var rating: CircleRatingView!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var searchImage: UIImageView!
    
    func setupImage(image: ImageReference?, placeholder: UIImage) {
        
        let filter = AspectScaledToFitSizeFilter(
            size: searchImage.frame.size
        )
        
        if let img = image {
            searchImage.af_setImage(
                withURL: img.url(width: searchImage.frame.width),
                placeholderImage: #imageLiteral(resourceName: "placeholder-movie"),
                filter: filter,
                imageTransition: .crossDissolve(0.3),
                completion: nil)
        } else {
            searchImage.image = placeholder
        }
    }
    
    func setupWithData(data: MovieStub) {
        title.text = data.title
        
        rating.percentage = CGFloat(data.score)
        rating.showRating(animated: false)
        subtitle.text = data.releaseYear?.getYear()
        
        setupImage(image: data.image, placeholder: #imageLiteral(resourceName: "placeholder-movie"))
    }


    func setupWithData(data: ActorListItem) {
        title.text = data.name
        rating.isHidden = true
        
        setupImage(image: data.image, placeholder: #imageLiteral(resourceName: "placeholderPerson"))
    }
    
    override func prepareForReuse() {
        searchImage.af_cancelImageRequest()
        searchImage.image = nil
    }
}
