//
//  SearchSource.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 18/09/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import Foundation
import UnboxedAlamofire
import Alamofire

protocol SearchSource {
    
    func searchMovies(query: String, completion: @escaping (APIResult<[APIMovieStub]>) -> Void)
    func searchActors(query: String, completion: @escaping (APIResult<[APIActorStub]>) -> Void)
}


class AlamoFireSearchSource: SearchSource {
    func searchMovies(query: String, completion: @escaping (APIResult<[APIMovieStub]>) -> Void) {
        Alamofire.request(Router.Search(query: query, searchType: .movie)).validate().responseObject() { (result:DataResponse<APIMovieResults>) in
            completion(result.asAPIResult() { $0.results } )
        }
    }
    
    func searchActors(query: String, completion: @escaping (APIResult<[APIActorStub]>) -> Void) {
        Alamofire.request(Router.Search(query: query, searchType: .person)).validate().responseObject() { (result:DataResponse<APIActorResults>) in
            completion(result.asAPIResult() { $0.results } )
        }
    }
}

