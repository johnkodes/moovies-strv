//
//  SearchViewModel.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 18/09/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import Foundation
import Alamofire

protocol SearchViewModelDelegate: class {
    
    func viewModelItemsUpdated()
    func viewModelChangedState(state: SearchViewModel.State)
    func viewModelShowActorDetail(id: String)
    func viewModelShowMovieDetail(id: String)
}

class SearchViewModel {
    
    enum SearchType {
        
        case movie
        case person
    }
    
    enum State {
        case empty
        case loading
        case ready
        case error
    }
    
    var error: Error?
    
    var state: State = .empty {
        didSet {
            if state != oldValue {
                delegate?.viewModelChangedState(state: state)
            }
        }
    }
    
    weak var delegate: SearchViewModelDelegate?
    
    let searchSource: SearchSource
    var searchQuery: String = ""
    var searchType: SearchType = .movie
    
    var movies: [MovieStub] = []
    var actors: [ActorListItem] = []
    
    func reloadResults(type: SearchType) {
        
        guard searchQuery != "" else {
            self.state = .empty
            return
        }

        if state == .loading {
            return
        }
        
        state = .loading
        
        switch type {
        case .movie:
            searchSource.searchMovies(query: searchQuery) { result in
                
                if let value = result.value {
                    
                    let items = value.sorted { $0.popularity > $1.popularity }.map {
                        MovieStub(
                            id: String($0.id),
                            title: $0.title,
                            image: $0.image,
                            popularity: $0.popularity,
                            score: $0.score,
                            releaseYear: $0.releaseDate
                        )
                    }
                    
                    self.movies = items
                    
                    self.state = items.isEmpty ? .empty : .ready
                    self.delegate?.viewModelItemsUpdated()
                    
                } else {
                    
                    guard let error = result.error else {
                        return
                    }
                    
                    if (error as NSError).code != -999 {
                        self.error = result.error
                        self.state = .error
                    }
                    
                }
            }
            
        case .person:
            searchSource.searchActors(query: searchQuery) { result in
                
                if let value = result.value {
                    
                    let items: [ActorListItem] = value.map {

                        ActorItem(
                            id: String($0.id),
                            name: $0.name,
                            character: nil,
                            movies: $0.knownFor.flatMap { $0.title },
                            image: $0.profileImage
                        )
                    }
                    
                    self.actors = items
                    
                    self.state = items.isEmpty ? .empty : .ready
                    self.delegate?.viewModelItemsUpdated()
            } else {
                
                self.error = result.error
                self.state = .error
            }
        }
        }
    }
    
    init(searchSource: SearchSource = AlamoFireSearchSource()) {
        self.searchSource = searchSource
    }
    
}
