//
//  SearchViewController.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 01/08/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import UIKit
import Alamofire

class SearchViewController: UIViewController, StoryboardInit {

    @IBOutlet weak var moviesButton: UIButton!
    @IBOutlet weak var actorsButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBAction func buttonTouchAction(_ sender: UIButton){
        if sender.tag == 1 {
            searchType = .movie
            selectButton(type: searchType)
            
        } else if sender.tag == 2 {
            searchType = .person
            selectButton(type: searchType)
        }
        
        viewModel.reloadResults(type: searchType)
    }
    
    var viewModel: SearchViewModel!
    var searchType: SearchViewModel.SearchType = .movie
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        viewModel = SearchViewModel()
        viewModel.delegate = self
        
        selectButton(type: searchType)
    }
    
    func selectButton(type: SearchViewModel.SearchType) {
        if type == .movie {
            moviesButton.isSelected = true
            actorsButton.isSelected = false
        } else {
            actorsButton.isSelected = true
            moviesButton.isSelected = false
        }
    }

}

extension SearchViewController: UISearchBarDelegate, SearchViewModelDelegate {
    
    func viewModelShowMovieDetail(id: String) {
        let moviesDetailViewController = MovieDetailViewController.storyboardInit()
        moviesDetailViewController.movieId = id
        
        self.navigationController?.pushViewController(moviesDetailViewController, animated: true)
    }
    
    func viewModelShowActorDetail(id: String) {
        
        let actorDetailViewController = ActorDetailViewController.storyboardInit()
        actorDetailViewController.actorId = id
        self.navigationController?.pushViewController(actorDetailViewController, animated: true)
    }
    
    
    func viewModelItemsUpdated() {
        tableView.reloadData()
        tableView.setContentOffset(CGPoint.zero, animated: true)
    }
    
    func viewModelChangedState(state: SearchViewModel.State) {
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if viewModel.searchQuery == "" {
             searchBar.text = ""
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        self.dismiss(animated: true, completion: nil)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionTasks, uploadTasks, downloadTasks) in
            sessionTasks.forEach({ $0.cancel() })
        }
        
        let query = searchBar.text
        
        guard
            let finalQuery = query?.replacingOccurrences(of: " ", with: "+")
            else {
                return
        }
        
        self.viewModel.searchQuery = finalQuery
        
        if finalQuery != "" {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(callRequest), object: nil)
            self.perform(#selector(callRequest), with: nil, afterDelay: 0.5)
        }
        
    }
    
    @objc func callRequest() {
        self.viewModel.reloadResults(type: self.searchType)
    }
    
    
    
}

extension SearchViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTableViewCell") as? SearchTableViewCell else {
            print("SearchTableViewCell not found: Creating empty table view cell")
            return UITableViewCell()
        }
        
        switch searchType {
        case .movie:
            cell.setupWithData(data: viewModel.movies[indexPath.row])
        case .person:
            cell.setupWithData(data: viewModel.actors[indexPath.row])
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch searchType {
        case .movie:
            viewModelShowMovieDetail(id: viewModel.movies[indexPath.row].id)
        case.person:
            viewModelShowActorDetail(id: viewModel.actors[indexPath.row].id)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch searchType {
        case .movie:
            return viewModel.movies.count
        case .person:
            return viewModel.actors.count
        }
        
    }
}

extension SearchViewController {
    
    func setupUI() {
        
        setupSearchBar()
        setupTableView()
        
        moviesButton.isSelected = true
    }
    
    func setupTableView() {
        
        let nib = UINib(nibName: "SearchTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "SearchTableViewCell")
        
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.keyboardDismissMode = .onDrag;

        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func setupSearchBar() {
        
        navigationController?.presentTransparentNavigationBar()
        
        let searchBar = UISearchBar();
        searchBar.showsCancelButton = true
        searchBar.placeholder = "Search for movie or actor"
        searchBar.delegate = self
        
        searchBar.backgroundColor = UIColor.init(named: "dark")
        searchBar.tintColor = UIColor.init(named: "dark")
        searchBar.barTintColor = UIColor.init(named: "dark")
        //searchBar.setSearchFieldBackgroundImage(UIImage.image(with: UIColor.init(rgbColorCodeRed: 34, green: 34, blue: 37, alpha: 1)), for: UIControlState.normal)
        searchBar.layer.cornerRadius = 0
        searchBar.isTranslucent = true
        
        guard let uiButton = searchBar.value(forKey: "cancelButton") as? UIButton else {
            return
        }
        
        uiButton.setTitle("CANCEL", for: .normal)
        //uiButton.addTextSpacing(spacing: 2)
        
        self.navigationItem.titleView = searchBar
    }
}
