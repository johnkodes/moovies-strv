//
//  FilterViewModel.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 11/09/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import Foundation

protocol FilterViewModelDelegate: class {
    
    func viewModelItemsUpdated()
}

protocol GenreListItem {
    
    var id: String { get }
    var name: String { get }
}

struct GenreItem: GenreListItem {
    
    var id: String
    var name: String
}

class FilterViewModel {
    
    weak var delegate: FilterViewModelDelegate?
    
    var genres = GenreManager.shared.genres
    var data: [GenreListItem] = []
    
    init() {
        if let genres = genres {
            for (_, value) in genres {
                data.append(GenreItem(id: "\(value.id)", name: value.name))
            }
        }
    }
    
    func getDataItem(index: Int) -> GenreListItem? {
        
        guard data.count > index else {
            
            return nil
        }
        
        return data[index]
    }
}
