//
//  FilterViewController.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 30/08/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import UIKit

protocol FilterViewDelegate: class {
    
    func filterValuesChanged(genres: [String], indexes: [IndexPath]?)
}

class FilterViewController: UIViewController, StoryboardInit {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func dismissAction() {
        
        var values: [String] = []
        let selected_indexPaths = tableView.indexPathsForSelectedRows
        
        if let selected_indexPaths = selected_indexPaths {
            for indexPath in selected_indexPaths {
                if let genreItem = viewModel.getDataItem(index: indexPath.row) {
                    values.append(genreItem.id)
                }
            }
        }
        
        if let delegate = delegate {
            delegate.filterValuesChanged(genres: values, indexes: selected_indexPaths)
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clearAction() {
        
        guard
            let selectedItems = tableView.indexPathsForSelectedRows
        else {
            print("no selected rows")
            return
        }
        
        for indexPath in selectedItems {
            tableView.deselectRow(at: indexPath, animated:true)
        }
    }
    
    // MARK: FilterViewController
    
    var viewModel: FilterViewModel!
    weak var delegate: FilterViewDelegate?
    
    var selectedIndexes: [IndexPath]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "FILTER"
        
        viewModel = FilterViewModel()
        
        let filterCell = UINib(nibName: "FilterTableViewCell", bundle: nil)
        tableView.register(filterCell, forCellReuseIdentifier: "FilterTableViewCell")
        
        viewModel.delegate = self
        
        tableView.delegate = self
        tableView.dataSource = self
        
        setupNavigationBar()
        
        // Set selected filter rows
        if let indexes = selectedIndexes {
            for index in indexes {
                tableView.selectRow(at: index, animated: true, scrollPosition: UITableViewScrollPosition.none)
            }
        }
    }
    
}

extension FilterViewController: FilterViewModelDelegate {
    
    func viewModelItemsUpdated() {
        
    }
}

extension FilterViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return viewModel.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard
            let genreItem = viewModel.getDataItem(index: indexPath.row),
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterTableViewCell") as? FilterTableViewCell else {
            
            print("FilterTableViewCell not found: Creating empty table view cell")
            return UITableViewCell()
        }
        
        cell.setupWithData(data: genreItem)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
    }
}

extension FilterViewController {
    
    func setupNavigationBar() {
        
        let cancel = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(dismissAction))
        cancel.image = #imageLiteral(resourceName: "icCancel")
        
        let clear = UIBarButtonItem(title: "CLEAR", style: .plain, target: self, action: #selector(clearAction))
        
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.backgroundColor = UIColor.init(named: "dark")
        navigationController?.navigationBar.barTintColor = UIColor.init(named: "dark")
        
        var attributes = [NSAttributedStringKey.font: UIFont(name: "Calibre-Semibold", size: 14) as Any, NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.kern: 2.0] as [NSAttributedStringKey : Any]
        
        navigationController?.navigationBar.titleTextAttributes = attributes
        navigationController?.navigationBar.setTitleVerticalPositionAdjustment(4, for: UIBarMetrics.default)
        
        attributes = [NSAttributedStringKey.font: UIFont(name: "Calibre-Regular", size: 13) as Any, NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.kern: 2.0]
        
        clear.setTitleTextAttributes(attributes as? [NSAttributedStringKey: NSObject], for: UIControlState.normal)
        
        self.navigationItem.leftBarButtonItem = cancel
        self.navigationItem.rightBarButtonItem = clear
    }
}
