//
//  FilterTableViewCell.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 30/08/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import UIKit

class FilterTableViewCell: UITableViewCell {

    @IBOutlet weak var genreTitle: UILabel!
    @IBOutlet weak var checkImage: UIImageView!
    
    func setupWithData(data: GenreListItem) {
        genreTitle.text = data.name
    }
    
    var color = UIColor.white
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        color = genreTitle.textColor
        checkImage.isHidden = true
        
        let bottomBorder = UIView.init(frame: CGRect(x: 0, y: self.bounds.height, width: self.bounds.width, height: 1))
        bottomBorder.backgroundColor = UIColor(rgbColorCodeRed: 255, green: 255, blue: 255, alpha: 0.1)
        
        //self.contentView.addSubview(bottomBorder)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        checkImage.isHidden = !selected
        genreTitle.textColor = selected ? UIColor.white : color
        
    }
    
}
