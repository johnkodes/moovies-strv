//
//  ActorsViewController.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 06/08/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import UIKit

class ActorsViewController: UIViewController, StoryboardInit {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var viewModel: ActorsViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let nib = UINib(nibName: "PersonCollectionViewCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "PersonCollectionViewCell")
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        viewModel = ActorsViewModel()
        
        viewModel.delegate = self
        viewModel.reloadActors()
        
        setupNavigationBar()
    }
    
    @IBAction func searchButtonAction() {
        
        let searchViewController = SearchViewController.storyboardInit()
        searchViewController.searchType = .person
        
        let navController = UINavigationController(rootViewController: searchViewController)
        self.navigationController?.present(navController, animated: true)
    }
    
    func setupNavigationBar () {
        
        let search = UIBarButtonItem(title: "Search", style: .plain, target: self, action: #selector(searchButtonAction))
        search.image = #imageLiteral(resourceName: "icSearch")
        
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.backgroundColor = UIColor.init(named: "dark")
        navigationController?.navigationBar.barTintColor = UIColor.init(named: "dark")
        
        self.navigationItem.rightBarButtonItem = search
        
        self.navigationController?.presentTransparentNavigationBar()
    }

}

extension ActorsViewController: ActorListViewModelDelegate {
    func viewModelItemsUpdated(items: [ActorListItem]) {
        collectionView.reloadData()
    }
    
    func viewModelChangedState(state: ActorsViewModel.State) {
    }
    
    func viewModelLoadDetail(actorId: String) {
        
        let actorDetailViewController = ActorDetailViewController.storyboardInit()
        actorDetailViewController.actorId = actorId
        self.navigationController?.pushViewController(actorDetailViewController, animated: true)
    }
    
}

extension ActorsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard
            let actor = viewModel.getDataItem(index: indexPath.row)
        else {
            return
        }
        
        //debugPrint(actor.id)
        viewModel.delegate?.viewModelLoadDetail(actorId: actor.id)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return viewModel.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard
            let actor = viewModel.getDataItem(index: indexPath.row),
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PersonCollectionViewCell", for: indexPath) as? PersonCollectionViewCell
        else {
            return UICollectionViewCell()
        }
        
        cell.setupWithData(data: actor)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfItemsPerRow = 2
        
        guard let flowLayout = collectionViewLayout as? UICollectionViewFlowLayout else {
            return CGSize(width: 0, height: 0)
        }
        
        let totalSpace = flowLayout.sectionInset.left + flowLayout.sectionInset.right + (flowLayout.minimumInteritemSpacing * CGFloat(numberOfItemsPerRow - 1))
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(numberOfItemsPerRow))
        
        return CGSize(width: size, height: 200)
    }
    
}
