//
//  ActorSource.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 01/09/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import Foundation
import Alamofire
import UnboxedAlamofire
import Unbox


protocol ActorSource {
    
    func fetchActorDetail(id: String, completion: @escaping (APIResult<APIActorFull>) -> Void)
    func fetchPopular(completion: @escaping (APIResult<[APIActorStub]>) -> Void)
    
}

class AlamofireActorSource: ActorSource {
    
    func fetchActorDetail(id: String, completion: @escaping (APIResult<APIActorFull>) -> Void) {
        Alamofire.request(Router.Person.Detail(personId: id)).validate().responseObject() { (result:DataResponse<APIActorFull>) in
            completion(result.asAPIResult())
        }
    }
    
    func fetchPopular(completion: @escaping (APIResult<[APIActorStub]>) -> Void) {
        Alamofire.request(Router.Person.Popular()).validate().responseObject() { (result:DataResponse<APIActorResults>) in
            completion(result.asAPIResult() { $0.results } )
        }
    }
    
}
