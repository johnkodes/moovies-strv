//
//  ActorsViewModel.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 07/08/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import Foundation
import UIKit

protocol ActorListViewModelDelegate: class {
    func viewModelItemsUpdated(items: [ActorListItem])
    func viewModelChangedState(state: ActorsViewModel.State)
    func viewModelLoadDetail(actorId: String)
}

class ActorsViewModel {
    
    enum State {
        case empty
        case loading
        case ready
        case error
    }
    
    weak var delegate: ActorListViewModelDelegate?
    let actorSource: ActorSource
    
    var error: Error?
    
    var items: [ActorListItem] = []
    
    var state: State = .empty {
        didSet {
            if state != oldValue {
                delegate?.viewModelChangedState(state: state)
            }
        }
    }
    
    init(actorSource: ActorSource = AlamofireActorSource()) {
        self.actorSource = actorSource
    }
    
    func reloadActors() {
        if state == .loading {
            return
        }
        
        state = .loading
        
        actorSource.fetchPopular() { result in
            
            if let value = result.value {
                
                self.items = value.map {
                    
                    let movies: [String] = $0.knownFor.flatMap {
                        if let title = $0.title {
                            return title
                        } else {
                            return $0.name
                        }
                    }
                
                    return ActorItem(id: String($0.id), name: $0.name, character: nil, movies: movies, image: $0.profileImage)
                }
                
                self.state = self.items.isEmpty ? .empty : .ready
                self.delegate?.viewModelItemsUpdated(items: self.items)
                
            } else {
                
                self.error = result.error
                self.state = .error
            }
            
        }
    }
    
    
    func getDataItem(index: Int) -> ActorListItem? {
        guard items.count > index else {
            
            return nil
        }
        
        return items[index]
    }
}
