//
//  MoviesViewController.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 01/08/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import UIKit

class MoviesViewController: UIViewController, StoryboardInit {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func filterButtonAction() {
        
        let filterViewController = FilterViewController.storyboardInit()
        filterViewController.delegate = self
        filterViewController.selectedIndexes = selectedFilterIndexes
        
        let navController = UINavigationController(rootViewController: filterViewController)
        self.navigationController?.present(navController, animated: true)
    }

    @IBAction func searchButtonAction() {
        
        let searchViewController = SearchViewController.storyboardInit()
        searchViewController.searchType = .movie
    
        let navController = UINavigationController(rootViewController: searchViewController)
        self.navigationController?.present(navController, animated: true)
    }

    
    var viewModel: MoviesViewModel!
    
    var selectedFilterIndexes: [IndexPath]?
    
    var genres: [String]? {
        didSet {
            viewModel.genres = genres
            viewModel.reloadMovies()
        }
    }
    
    let cells: [MoviesTableViewSectionType] = [.featured, .popular, .playing]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        viewModel = MoviesViewModel()
        viewModel.delegate = self
        //viewModelChangedState(state: viewModel.state)
        
        viewModel.reloadMovies()
    }

}

extension MoviesViewController: MovieListViewModelDelegate, FilterViewDelegate {
    
    func viewModelChangedState(state: MoviesViewModel.State) {
        tableView.reloadData()
        tableView.setContentOffset(CGPoint.zero, animated: true)
        tableView.backgroundView = nil
        
        if state == .empty {
            tableView.setupEmptyBackgroundView()
        } else if state == .loading {
            // set tableview insets
        }
    }
    
    func removeOverlay() {
        guard let view = self.view.viewWithTag(88) else {
            return
        }
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .transitionCrossDissolve, animations: {
            view.alpha = 0
        }, completion: { finished in
            view.removeFromSuperview()
        })
    }
    
    func viewModelItemsUpdated() {
        tableView.reloadData()
    }
    
    func viewModelLoadDetail(movieId: String) {
        
        let moviesDetailViewController = MovieDetailViewController.storyboardInit()
        moviesDetailViewController.movieId = movieId
        
        self.navigationController?.pushViewController(moviesDetailViewController, animated: true)
    }
    
    func filterValuesChanged(genres: [String], indexes: [IndexPath]?) {
        self.genres = genres
        self.selectedFilterIndexes = indexes
        
        tableView.setContentOffset(CGPoint.zero, animated: true)
        
        let indexPath = IndexPath(row: 0, section: 0)
        tableView.scrollToRow(at: indexPath, at: .top, animated: true)

    }
}

extension MoviesViewController: UITableViewDataSource, UITableViewDelegate {
    
    // MARK: Table View Cells
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch viewModel.state {
            
        case .empty:
            return UITableViewCell()
            
        case .loading, .error:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "LoadingTableViewCell") as? LoadingTableViewCell else {
                return UITableViewCell()
            }
            
            return cell
            
        case .ready:
        
            if cells[indexPath.section] == .featured {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "MoviesSlideshowTableViewCell") as? MoviesSlideshowTableViewCell else {
                    print("MoviesSlideshowTableViewCell not found: Creating empty table view cell")
                    return UITableViewCell()
                }
                
                cell.collectionView.tag = indexPath.section
                cell.viewModel = viewModel
                
                cell.collectionView.reloadData()
                
                return cell
                
            } else {
                
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "MoviesTableCell") as? MoviesTableViewCell else {
                    print("MoviesTableCell not found: Creating empty table view cell")
                    return UITableViewCell()
                }
                
                cell.collectionView.tag = indexPath.section
                cell.viewModel = viewModel
                
                cell.collectionView.reloadData()
                
                return cell
            }
        }
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        switch viewModel.state {
            
        case .ready:
            return viewModel.data.count
            
        case .empty:
            return 0
            
        default:
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if viewModel.state == .loading {
            return tableView.frame.height
        } else {
            return 200
        }
    }
    
    // MARK: Table View Section Header
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        guard let section = viewModel.getSection(index: section) else {
            
            return UITableViewCell()
        }
        
        guard let headerCell = tableView.dequeueReusableCell(withIdentifier: "MoviesTableHeaderCell") as? MoviesTableViewHeaderCell else {
            print("MoviesTableHeaderCell not found")
            return UITableViewCell()
        }
        
        headerCell.setupWithData(data: section)
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        guard let section = viewModel.getSection(index: section) else {
            return 0
        }
            
        if section.type == .featured {
            return 1
        } else {
            return 64
        }
    }
}

extension MoviesViewController {
    
    func setupUI() {
        setupNavigationBar()
        setupTableView()
    }
    
    func setupTableView() {
        
        var nib = UINib(nibName: "MoviesTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "MoviesTableCell")
        
        nib = UINib(nibName: "MoviesSlideshowTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "MoviesSlideshowTableViewCell")
        
        nib = UINib(nibName: "MoviesTableViewHeaderCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "MoviesTableHeaderCell")
        
        nib = UINib(nibName: "LoadingTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "LoadingTableViewCell")
        
        tableView.separatorStyle = .none
        
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func setupNavigationBar() {
        
        let filter = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(filterButtonAction))
        filter.image = #imageLiteral(resourceName: "icSettings")
        
        let search = UIBarButtonItem(title: "Search", style: .plain, target: self, action: #selector(searchButtonAction))
        search.image = #imageLiteral(resourceName: "icSearch")
        
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.backgroundColor = UIColor.init(named: "dark")
        navigationController?.navigationBar.barTintColor = UIColor.init(named: "dark")
        
        self.navigationItem.rightBarButtonItem = search
        self.navigationItem.leftBarButtonItem = filter
        
    }
}
