//
//  MovieSource.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 24/08/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import Foundation
import Alamofire
import UnboxedAlamofire
import Unbox


protocol MovieSource {
    
    func fetchMovieDetail(id: String, completion: @escaping (APIResult<APIMovieFull>) -> Void)
    func fetchNowPlaying(genres: [String]?, completion: @escaping (APIResult<[APIMovieStub]>) -> Void)
    func fetchPopular(genres: [String]?, completion: @escaping (APIResult<[APIMovieStub]>) -> Void)
    func fetchFeatured(genres: [String]?, completion: @escaping (APIResult<[APIMovieStub]>) -> Void)
}

class AlamofireMovieSource: MovieSource {
    
    func fetchMovieDetail(id: String, completion: @escaping (APIResult<APIMovieFull>) -> Void) {
        Alamofire.request(Router.Movie.Detail(movieId: id)).validate().responseObject() { (result:DataResponse<APIMovieFull>) in
            completion(result.asAPIResult())
        }
    }
    
    func fetchNowPlaying(genres: [String]?, completion: @escaping (APIResult<[APIMovieStub]>) -> Void) {
        Alamofire.request(Router.Movie.Discover(type: .nowPlaying, genres: genres?.joined(separator: ", ") ?? "")).validate().responseObject() { (result:DataResponse<APIMovieResults>) in
            completion(result.asAPIResult() { $0.results } )
        }
    }
    
    func fetchPopular(genres: [String]?, completion: @escaping (APIResult<[APIMovieStub]>) -> Void) {
        Alamofire.request(Router.Movie.Discover(type: .popular, genres: genres?.joined(separator: ", ") ?? "")).validate().responseObject() { (result:DataResponse<APIMovieResults>) in
            completion(result.asAPIResult() { $0.results } )
        }
    }
    
    func fetchFeatured(genres: [String]?, completion: @escaping (APIResult<[APIMovieStub]>) -> Void) {
        Alamofire.request(Router.Movie.Discover(type: .featured, genres: genres?.joined(separator: ", ") ?? "")).validate().responseObject() { (result:DataResponse<APIMovieResults>) in
            completion(result.asAPIResult() { $0.results } )
        }
    }
    
}
