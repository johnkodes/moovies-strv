//
//  MoviesTableViewHeaderCell.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 08/08/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import UIKit

class MoviesTableViewHeaderCell: UITableViewCell {
    @IBOutlet weak var sectionLabel: UILabel!
    @IBOutlet weak var showButton: UIButton!
    
    @IBAction func showAllAction(_ sender: UIButton) {
    }
    
    func setupWithData(data: MoviesTableViewSection) {
        
        sectionLabel.text = data.type.rawValue
        //showButton.addTextSpacing(spacing: 2)
        showButton.isHidden = true
    }
}
