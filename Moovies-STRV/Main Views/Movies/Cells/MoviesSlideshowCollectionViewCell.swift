//
//  MoviesSlideshowCollectionViewCell.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 11/09/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import UIKit
import AlamofireImage

class MoviesSlideshowCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupWithData(data: MovieStubFeatured) {
        
        genreLabel.text = data.genres.first?.uppercased()
        genreLabel.addTextSpacing(spacing: 2)
        titleLabel.text = data.title
        
        if data.title.count > 30 {
            titleLabel.font.withSize(30)
        }
        
        let filter = AspectScaledToFillSizeFilter(
            size: image.frame.size
        )
        
        if let img = data.image {
            image.af_setImage(
                withURL: img.url(width: image.frame.width),
                placeholderImage: #imageLiteral(resourceName: "placeholder-movie"),
                filter: filter,
                imageTransition: .crossDissolve(0.3),
                completion: nil)
            
        } else {
            image.image = #imageLiteral(resourceName: "trailerPlaceholder")
            let overlay = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
            overlay.frame = image.frame
            
            image.addSubview(overlay)
            image.setupImageOverlay()
        }
    }
    
    override func prepareForReuse() {
        image.af_cancelImageRequest()
        image.image = nil
    }

}
