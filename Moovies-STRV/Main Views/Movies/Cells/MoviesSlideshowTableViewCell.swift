//
//  MoviesSlideshowTableViewCell.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 11/09/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import UIKit

class MoviesSlideshowTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var viewModel = MoviesViewModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.showsHorizontalScrollIndicator = false
        
        let nib = UINib(nibName: "MoviesSlideshowCollectionViewCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "MoviesSlideshowCollectionViewCell")
    }
}

extension MoviesSlideshowTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard
            let section = viewModel.getSection(index: collectionView.tag),
            let dataItem = section.getDataItem(index: indexPath.row)
        else {
            return
        }
        
        viewModel.delegate?.viewModelLoadDetail(movieId: dataItem.id)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        guard
            viewModel.data.count > section
        else {
            return 0
        }
        
        return viewModel.data[collectionView.tag].data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = UIScreen.main.bounds.size.width
        return CGSize(width: cellWidth, height: 250)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard
            let section = viewModel.getSection(index: collectionView.tag),
            let dataItem = section.getDataItem(index: indexPath.row) as? MovieStubFeatured
        else {
            return UICollectionViewCell()
        }
        
        guard
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MoviesSlideshowCollectionViewCell", for: indexPath) as? MoviesSlideshowCollectionViewCell
        else {
            print("MovieCollectionCell not found. Creating empty collection view cell")
            return UICollectionViewCell()
        }
        
        self.pageControl.currentPage = 0
        self.pageControl.numberOfPages = section.data.count
        
        cell.setupWithData(data: dataItem)
        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
}
