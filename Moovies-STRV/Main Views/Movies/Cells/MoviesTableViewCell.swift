//
//  MoviesTableViewCell.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 07/08/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import UIKit

class MoviesTableViewCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    
    var viewModel = MoviesViewModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        let nib = UINib(nibName: "MovieCollectionViewCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "MovieCollectionCell")
    }
}

extension MoviesTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
        guard
            let section = viewModel.getSection(index: collectionView.tag),
            let dataItem = section.getDataItem(index: indexPath.row)
        else {
            return
        }
        
        viewModel.delegate?.viewModelLoadDetail(movieId: dataItem.id)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        guard viewModel.data.count > section else {
            
            return 0
        }
        
        print("Collection View Tag: \(collectionView.tag)")
        
        return viewModel.data[collectionView.tag].data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard
            let section = viewModel.getSection(index: collectionView.tag),
            let dataItem = section.getDataItem(index: indexPath.row)
        else {
            
            return UICollectionViewCell()
        }
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieCollectionCell", for: indexPath) as? MovieCollectionViewCell else {
            
            print("MovieCollectionCell not found. Creating empty collection view cell")
            return UICollectionViewCell()
        }
        
        cell.setupWithData(data: dataItem)
        
        return cell
    }
}
