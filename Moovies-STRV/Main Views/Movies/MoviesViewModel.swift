//
//  MoviesViewModel.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 08/08/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import Foundation

protocol MovieListItem {
    var id: String { get }
    var title: String { get }
    var image: ImageReference? { get }
}

struct MovieStub: MovieListItem {
    
    var id: String
    var title: String
    var image: ImageReference?
    var popularity: Int
    var score: Float
    var releaseYear: Date?
}

struct MovieStubFeatured: MovieListItem {
    
    var id: String
    var image: ImageReference?
    var genres: [String]
    var title: String
}

protocol MovieListViewModelDelegate: class {
    func viewModelItemsUpdated()
    func viewModelChangedState(state: MoviesViewModel.State)
    func viewModelLoadDetail(movieId: String)
}

class MoviesViewModel {
    
    enum State {
        case empty
        case loading
        case ready
        case error
    }
    
    weak var delegate: MovieListViewModelDelegate?
    
    let movieSource: MovieSource
    
    var genres: [String]?
    var data: [MoviesTableViewSection] = []
    
    var error: Error?
    
    var state: State = .empty {
        didSet {
            if state != oldValue {
                delegate?.viewModelChangedState(state: state)
            }
        }
    }
    
    init(movieSource: MovieSource = AlamofireMovieSource()) {
        self.movieSource = movieSource
    }
    
    func reloadMovies() {
        
        self.data = []
        
        if state == .loading {
            return
        }
        
        state = .loading
        
        movieSource.fetchFeatured(genres: genres) { result in
            
            if let value = result.value {
                
                let sorted = value.sorted { $0.popularity > $1.popularity }
                let maxValue = sorted.count
                let numberOfItems = min(maxValue, 3)
                
                let items = sorted[0..<numberOfItems].map {
                    MovieStubFeatured(
                        id: String($0.id),
                        image: $0.image,
                        genres: GenreManager.shared.map(ids: $0.genreIds).map { $0.name },
                        title: $0.title)
                }   
                
                self.data +=
                    [MoviesTableViewSection(
                        type: .featured,
                        data: items
                        )]
                
                self.movieSource.fetchPopular(genres: self.genres) { result in
                    
                    if let value = result.value {
                        
                        let items = value.map {
                            MovieStub(id: String($0.id), title: $0.title, image: $0.image, popularity: $0.popularity, score: $0.score, releaseYear: $0.releaseDate)
                        }
                        
                        self.data +=
                            [MoviesTableViewSection(
                                type: .popular,
                                data: items
                                )]
                        
                        self.movieSource.fetchNowPlaying(genres: self.genres) { result in
                            if let value = result.value {
                                
                                let items = value.map {
                                    MovieStub(id: String($0.id), title: $0.title, image: $0.image, popularity: $0.popularity, score: $0.score, releaseYear: $0.releaseDate)
                                }
                                
                                self.data +=
                                    [MoviesTableViewSection(
                                        type: .playing,
                                        data: items
                                        )]
                                
                                self.state = items.isEmpty ? .empty : .ready
                                self.delegate?.viewModelItemsUpdated()
                                
                            } else {
                                
                                self.error = result.error
                                self.state = .error
                            }
                        }
                        
                    } else {
                        
                        self.error = result.error
                        self.state = .error
                    }
                }
            }
            
        }
        
    }
    
    func getSection(index: Int) -> MoviesTableViewSection? {
        
        guard data.count > index else {
            
            return nil
        }
        
        return data[index]
    }
}

enum MoviesTableViewSectionType: String {
    
    case featured
    case popular = "Most Popular"
    case playing = "Now Playing"
}


class MoviesTableViewSection {
    
    init(type: MoviesTableViewSectionType, data: [MovieListItem]) {
        self.type = type
        self.data = data
    }
    
    let type: MoviesTableViewSectionType
    var data: [MovieListItem]
    
    func getDataItem(index: Int) -> MovieListItem? {
        
        guard data.count > index else {
            
            return nil
        }
        
        return data[index]
    }
}
