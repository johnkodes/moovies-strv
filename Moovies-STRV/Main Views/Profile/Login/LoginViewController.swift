//
//  LoginViewController.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 28/07/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import UIKit
import KeychainAccess

protocol LoginViewControllerDelegate: class {
    func authenticated(token: String, authenticated: Bool)
}

class LoginViewController: UIViewController, LoginViewControllerDelegate, StoryboardInit {
    @IBOutlet weak var login: UIButton!
    
    @IBAction func loginAction(_ sender: Any) {
        
        authenticationSource.requestToken() { result in
            
            if let value = result.value {
                
                let webViewController = WebViewController()
                webViewController.delegate = self
                webViewController.token = value.requestToken
                webViewController.targetURL = URL(string: "https://www.themoviedb.org/authenticate/\(value.requestToken)")
                
                self.present(webViewController, animated: true, completion: nil)
                //self.state = items.isEmpty ? .empty : .ready
                //self.delegate?.viewModelChangedState()
                
            } else {
                
                //self.error = result.error
                //self.state = .error
            }
        }
    }
    
    var authenticationSource: AuthenticationSource = AlamofireAuthenticationSource()
    var delegate: ProfileViewControllerDelegate?
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.presentTransparentNavigationBar()
    }
    
    func authenticated(token: String, authenticated: Bool) {
        if authenticated {
            authenticationSource.createSession(requestToken: token) { result in
                if let value = result.value {
                    
                    if value.success {
                        let keychain = Keychain(service: Settings.keychainService)
                        
                        do {
                            try keychain.set(value.sessionId, key: "session_id")
                        }
                        catch let error {
                            print(error)
                        }
                        
                        UserDefaults.standard.set(true, forKey: "isLogged")
                    }
                    
                }
                
            }
        }
        
        delegate?.logged()
    }
}
