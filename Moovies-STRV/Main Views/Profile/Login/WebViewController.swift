//
//  WebViewController.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 19/09/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import Foundation
import UIKit

class WebViewController: UIViewController, UIWebViewDelegate {
    
    var token: String = ""
    var targetURL: URL?
    var webView: UIWebView = UIWebView()
    var delegate: LoginViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.delegate = self
        webView.frame = view.bounds
        webView.scalesPageToFit = true
        view.addSubview(webView)
        
        loadAddressURL()
        
        if let cookies = HTTPCookieStorage.shared.cookies {
            for cookie in cookies {
                NSLog("\(cookie)")
            }
        }
        
        let storage = HTTPCookieStorage.shared
        for cookie in storage.cookies! {
            //storage.deleteCookie(cookie)
        }
    }
    
    func loadAddressURL() {
        
        if let targetURL = targetURL {
            let request = URLRequest(url: targetURL)
            webView.loadRequest(request)
        }
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        let headers = webView.request?.allHTTPHeaderFields
        for (key,value) in headers! {
            print("key \(key) value \(value)")
        }
        
        if let url = webView.request?.url?.absoluteString{
            if url.contains("allow") {
                
                delegate?.authenticated(token: token, authenticated: true)
                self.dismiss(animated: true, completion: nil)
                
            } else if url.contains("deny") {
                
                delegate?.authenticated(token: "", authenticated: false)
                
                let alertPresenter = AlertPresenter()
                let alert = UIAlertController(title: "Error", message: "You have to allow the permissions in order to login.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default) {_ in self.dismiss(animated: true, completion: nil)})
                
                alertPresenter.presentAlertController(controller: alert, fromController: self)
            }
        }
    }
}
