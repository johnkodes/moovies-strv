//
//  AuthenticationViewModel.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 19/09/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import Foundation

protocol AuthenticationViewModelDelegate: class {
    
    func viewModelChangedState(state: AuthenticationViewModel.State)
    func viewModelLoggedIn()
    func authenticated()
}

class AuthenticationViewModel {
    
    enum State {
        case empty
        case loading
        case ready
        case error
    }
    
    var error: Error?
    
    var state: State = .empty {
        didSet {
            if state != oldValue {
                delegate?.viewModelChangedState(state: state)
            }
        }
    }
    
    weak var delegate: AuthenticationViewModelDelegate?
    
    
    var requestToken: String?
    var authenticated = false {
        didSet {
            if authenticated {
                delegate?.authenticated()
            }
        }
    }
    
    let authenticationSource: AuthenticationSource
    
    init(authenticationSource: AuthenticationSource = AlamofireAuthenticationSource()) {
        self.authenticationSource = authenticationSource
    }
}
