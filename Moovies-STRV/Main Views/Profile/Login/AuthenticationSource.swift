//
//  LoginSource.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 19/09/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import Foundation
import Alamofire
import UnboxedAlamofire

protocol AuthenticationSource {
    
    func requestToken(completion: @escaping (APIResult<APIToken>) -> Void)
    func createSession(requestToken: String, completion: @escaping (APIResult<APISession>) -> Void)
}

class AlamofireAuthenticationSource: AuthenticationSource {
    
    func requestToken(completion: @escaping (APIResult<APIToken>) -> Void) {
        
        Alamofire.request(Router.Authentication.RequestToken()).validate().responseObject() { (result:DataResponse<APIToken>) in
            completion(result.asAPIResult())
        }
    }
    
    func createSession(requestToken: String, completion: @escaping (APIResult<APISession>) -> Void) {
        
        Alamofire.request(Router.Authentication.CreateSession(requestToken: requestToken)).validate().responseObject() { (result:DataResponse<APISession>) in
            completion(result.asAPIResult())
        }
    }
}
