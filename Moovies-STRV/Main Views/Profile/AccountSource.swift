//
//  AccountSource.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 19/09/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import Foundation
import UnboxedAlamofire
import Alamofire

protocol AccountSource: class {
    
    func fetchAccountDetails(sessionId: String, completion: @escaping (APIResult<APIAccount>) -> Void)
    func fetchFavoriteMovies(sessionId: String, accountId: String, completion: @escaping (APIResult<[APIMovieStub]>) -> Void)
}

class AlamofireAccountSource: AccountSource {
    func fetchAccountDetails(sessionId: String, completion: @escaping (APIResult<APIAccount>) -> Void) {
        Alamofire.request(Router.Account.Detail(sessionId: sessionId)).validate().responseObject() { (result:DataResponse<APIAccount>) in
            completion(result.asAPIResult())
        }
    }
    
    func fetchFavoriteMovies(sessionId: String, accountId: String, completion: @escaping (APIResult<[APIMovieStub]>) -> Void) {
        Alamofire.request(Router.Account.Favorites(sessionId: sessionId, accountId: accountId)).validate().responseObject() { (result:DataResponse<APIMovieResults>) in
            completion(result.asAPIResult() { $0.results } )
        }
    }
    
}
