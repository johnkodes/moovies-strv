//
//  ProfileViewController.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 19/09/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import UIKit
import AlamofireImage
import KeychainAccess

protocol ProfileViewControllerDelegate {
    
    func logged()
}

class ProfileViewController: UIViewController, StoryboardInit, ProfileViewControllerDelegate {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    var loginVc: LoginViewController!
    
    var viewModel: ProfileViewModel!
    var profile: ProfileItem? {
        didSet {
            
            guard let profile = profile else { return }
            
            let filter = AspectScaledToFillSizeFilter(
                size: image.frame.size
            )
            
            let avatars = ["mm", "identicon", "monsterid", "wavatar", "retro"]
            let dice = arc4random_uniform(4)
            
            let imageURL = URL(string: profile.image.absoluteString + "?s=200&d=\(avatars[Int(dice)])")!
            
            image.af_setImage(
                withURL: imageURL,
                placeholderImage: #imageLiteral(resourceName: "icProfileTapbar"),
                filter: filter,
                imageTransition: .crossDissolve(0.2),
                completion: nil)
            
            nameLabel.text = profile.name != "" ? profile.name : profile.username
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        viewModel = ProfileViewModel()
        viewModel.delegate = self
        
        loginVc = LoginViewController.storyboardInit()
        
        if UserDefaults.standard.bool(forKey: "isLogged") {
            viewModel.reloadProfile()
        } else {
            viewModel.state = .login
        }
        
        viewModelChangedState(state: viewModel.state)
    }
    

    func logOutUser() {
        
        let keychain = Keychain(service: Settings.keychainService)
        keychain["session_id"] = nil
        UserDefaults.standard.set(false, forKey: "isLogged")
        viewModel.state = .login
    }
    
    func logged() {
        viewModel.state = .ready
        viewModelChangedState(state: .ready)
    }
    
    @objc func logoutActionSheet() {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let logout = UIAlertAction(title: "Log out", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in self.logOutUser() })
        logout.setValue(UIColor.init(named: "red"), forKey: "titleTextColor")
        actionSheet.addAction(logout)
        
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
        cancel.setValue(UIColor.init(named: "dark"), forKey: "titleTextColor")
        actionSheet.addAction(cancel)
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
}

extension ProfileViewController: ProfileViewModelDelegate {
    
    func viewModelItemUpdated(item: ProfileItem) {
        profile = item
        tableView.reloadData()
    }
    
    func viewModelChangedState(state: ProfileViewModel.State) {
        
        let overlayView = UIScreen.main.snapshotView(afterScreenUpdates: false)
        overlayView.tag = 88
        let overlay = UIVisualEffectView()
        
        switch state {
        case .error:
            
            let alertPresenter = AlertPresenter()
            let alert = UIAlertController(title: "Error", message: viewModel.error?.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .default) {_ in
                //self.dismiss(animated: true, completion: nil)
            })
            
            alert.addAction(UIAlertAction(title: "Try again", style: .default) {_ in
                //self.dismiss(animated: true, completion: nil)
                self.viewModel.reloadProfile()
            })
            
            alertPresenter.presentAlertController(controller: alert, fromController: self)
            
        case .login:
            removeBlurOverlay()
            navigationItem.rightBarButtonItem = nil
            self.addChildViewController(self.loginVc)
            loginVc.delegate = self
            loginVc.view.tag = 99
            loginVc.view.alpha = 1
            
            self.view.addSubview(loginVc.view)
            loginVc.didMove(toParentViewController: self)
            
        case .ready, .empty:
            removeBlurOverlay()
            loginVc.removeFromParentViewController()
            loginVc.view.removeFromSuperview()
            setupNavigationBar()
            
        case .loading:
            overlayView.frame = self.view.frame
            overlay.frame = overlayView.frame
            overlay.effect = UIBlurEffect(style: .dark)
            
            self.view.addSubview(overlayView)
            overlayView.addSubview(overlay)
            
            removeBlurOverlay()
        }
        
    }
    
    func removeBlurOverlay() {
        guard let view = self.view.viewWithTag(88) else {
            return
        }
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .transitionCrossDissolve, animations: {
            view.alpha = 0
        }, completion: { finished in
            view.removeFromSuperview()
        })
    }
    
    func viewModelLoadDetail(movieId: String) {
        
        let moviesDetailViewController = MovieDetailViewController.storyboardInit()
        moviesDetailViewController.movieId = movieId
        
        self.navigationController?.pushViewController(moviesDetailViewController, animated: true)
    }
    
}

extension ProfileViewController: UITableViewDataSource, UITableViewDelegate {
    
    // MARK: Table View Cells
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileMovieTableViewCell") as? ProfileMovieTableViewCell else {
            print("ProfileMovieTableViewCell not found: Creating empty table view cell")
            return UITableViewCell()
        }
            
        cell.collectionView.tag = indexPath.section
        cell.viewModel = viewModel
            
        cell.collectionView.reloadData()
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 200
    }

}

extension ProfileViewController {
    
    func setupUI() {
        setupNavigationBar()
        setupTableView()
        
        navigationController?.presentTransparentNavigationBar()
        
        image.setupImageOverlay()
    }
    
    func setupNavigationBar () {
        
        let logout = UIBarButtonItem(title: "Search", style: .plain, target: self, action: #selector(logoutActionSheet))
        logout.image = #imageLiteral(resourceName: "icOptions")
        
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.backgroundColor = UIColor.init(named: "dark")
        navigationController?.navigationBar.barTintColor = UIColor.init(named: "dark")
        
        self.navigationItem.rightBarButtonItem = logout
    }
    
    func setupTableView() {
        
        let nib = UINib(nibName: "ProfileMovieTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "ProfileMovieTableViewCell")
        
        tableView.separatorStyle = .none
        
        tableView.dataSource = self
        tableView.delegate = self
    }

}

