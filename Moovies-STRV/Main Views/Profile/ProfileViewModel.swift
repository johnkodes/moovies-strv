//
//  ProfileViewModel.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 19/09/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import Foundation
import KeychainAccess

protocol ProfileListItem {
    
    var id: String { get }
    var name: String { get }
    var username: String { get }
    var image: URL { get }
}

struct ProfileItem: ProfileListItem {
    
    var id: String
    var name: String
    var username: String
    var image: URL
    var favorites: [MovieListItem]
}

protocol ProfileViewModelDelegate: class {
   
    func viewModelItemUpdated(item: ProfileItem)
    func viewModelChangedState(state: ProfileViewModel.State)
    func viewModelLoadDetail(movieId: String)
}

class ProfileViewModel {
    
    enum State {
        case empty
        case loading
        case ready
        case error
        case login
    }
    
    weak var delegate: ProfileViewModelDelegate?
    
    let accountSource: AccountSource
    
    var profile: ProfileItem?
    
    var error: Error?
    
    var state: State = .empty {
        didSet {
            if state != oldValue {
                delegate?.viewModelChangedState(state: state)
            }
        }
    }
    
    func reloadProfile() {
        if state == .loading {
            return
        }
        
        state = .loading
        
        let keychain = Keychain(service: Settings.keychainService)
        
        guard let sessionId = keychain["session_id"] else {
            UserDefaults.standard.set(false, forKey: "isLogged")
            print("Session not found")
            return
        }
        
        accountSource.fetchAccountDetails(sessionId: sessionId){ result in
            if let profile = result.value {
                
                self.accountSource.fetchFavoriteMovies(sessionId: sessionId, accountId: profile.id) { result in
                    
                    self.profile = ProfileItem(
                        id: profile.id,
                        name: profile.name,
                        username: profile.username,
                        image: profile.avatar.fullURL,
                        favorites: [])
                    
                    if let profile = self.profile {
                        self.delegate?.viewModelItemUpdated(item: profile)
                    }
                    
                    if let favorites = result.value {
                        self.profile?.favorites = favorites.map { MovieStub(id: $0.id, title: $0.title, image: $0.image, popularity: $0.popularity, score: $0.score, releaseYear: $0.releaseDate) }
                        
                        if let profile = self.profile {
                            self.delegate?.viewModelItemUpdated(item: profile)
                        }
                        
                    } else {
                        
                        self.error = result.error
                        self.state = .error
                    }
                }
            } else {
                
                self.error = result.error
                self.state = .error
            }
            
            self.state = self.profile != nil ? .empty : .ready
            
        }
        
    }
    
    init(accountSource: AccountSource = AlamofireAccountSource()) {
        self.accountSource = accountSource
        state = UserDefaults.standard.bool(forKey: "isLogged") ? .ready : .login
        delegate?.viewModelChangedState(state: state)
    }
}
