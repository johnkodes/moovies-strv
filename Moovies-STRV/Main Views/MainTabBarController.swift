//
//  MainTabBarController.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 07/08/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController, StoryboardInit {

    override func viewDidLoad() {
        super.viewDidLoad()

        let movieViewController = MoviesViewController.storyboardInit()
        movieViewController.tabBarItem.title = "Movies"
        
        let actorsViewController = ActorsViewController.storyboardInit()
        actorsViewController.tabBarItem.title = "Actors"
        
        let profileViewController = ProfileViewController.storyboardInit()
        profileViewController.tabBarItem.title = "Profile"
    
        movieViewController.tabBarItem.image = #imageLiteral(resourceName: "icMoviesTapbar")
        actorsViewController.tabBarItem.image = #imageLiteral(resourceName: "icActorsTapbar")
        profileViewController.tabBarItem.image = #imageLiteral(resourceName: "icProfileTapbar")
        
        let viewControllersList = [movieViewController, actorsViewController, profileViewController]
        viewControllers = viewControllersList.map { UINavigationController(rootViewController: $0) }
        
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.presentTransparentNavigationBar()
        
        /*let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionFade
        self.navigationController?.view.layer.add(transition, forKey:nil) */
    }
    
}
