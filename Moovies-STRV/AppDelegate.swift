//
//  AppDelegate.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 28/07/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireNetworkActivityIndicator
import ReachabilitySwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        setupUI()
        
        _ = GenreManager.shared
        
        NetworkActivityIndicatorManager.shared.isEnabled = true
        
        //Alamofire.SessionManager.default.adapter = DebugRequestAdapter()
        
        let reachability = Reachability()!
        reachability.whenReachable = { reachability in
            if reachability.isReachableViaWiFi {
                Settings.pixelDensity = 2
            } else {
                Settings.pixelDensity = 1
            }
        }
        
        reachability.stopNotifier()
        
        return true
    }
    
    func setupUI() {
        
        UINavigationBar.appearance().backgroundColor = UIColor.init(named: "dark")
        UINavigationBar.appearance().tintColor = UIColor.white
        
        let attributes = [
            NSAttributedStringKey.font: UIFont(name: "Calibre-Regular", size: 19),
            NSAttributedStringKey.foregroundColor: UIColor.white
        ]
        
        let backImage = UIImage(named: "navBarBackButton")?.withRenderingMode(.alwaysOriginal)
        UINavigationBar.appearance().backIndicatorImage = backImage
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = backImage
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -80.0), for: .default)
        
        UIBarButtonItem.appearance().setTitleTextAttributes(attributes, for: UIControlState.normal)
        
        let attributes1 = [
            NSAttributedStringKey.foregroundColor: UIColor.white,
            NSAttributedStringKey.font: UIFont(name: "Calibre-Medium", size: 13)!,
            NSAttributedStringKey.kern: 2.0
            ] as [NSAttributedStringKey : Any]
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes(attributes1, for: .normal)
        
        UIApplication.shared.statusBarStyle = .lightContent
        UIApplication.shared.isStatusBarHidden = false
    }
    
    // TODO: Fix layout on MediaViewController
    /*func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        if let navigationController = self.window?.rootViewController as? UITabBarController {
            if navigationController.presentedViewController is MediaViewController {
                return UIInterfaceOrientationMask.all
            } else {
                return UIInterfaceOrientationMask.portrait
            }
        }
        return UIInterfaceOrientationMask.portrait
    } */
}

