//
//  Constants.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 12/08/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import Foundation

enum Settings {
    
    static let baseURL: String = "https://api.themoviedb.org/3/"
    static let apiKey: String = "c361d02c5dfff7d63adfafbb344088f9"
    static let apiImageLanguage = "en,null"
    static let apiRegion = "US"
    static let apiLanguage: String = "en-US"
    static let keychainService: String = "com.jankodes.Moovies"
    static let gravatarBaseUrl = "https://www.gravatar.com/avatar/"
    static var pixelDensity: Int = 2
}
