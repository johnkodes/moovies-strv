//
//  UIView+Snapshot.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 20/09/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func snapshotView() -> UIView? {
        guard let image = snapshotImage() else { return nil }
        return UIImageView(image: image)
    }
    
    func snapshotImage() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(bounds.size, isOpaque, contentScaleFactor)
        defer { UIGraphicsEndImageContext() }
        
        drawHierarchy(in: bounds, afterScreenUpdates: false)
        
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}
