//
//  UIImage+Blur.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 20/09/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    
    func addBlur() {
        
        let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
        visualEffectView.frame = self.bounds
        
        self.addSubview(visualEffectView)
    }
}
