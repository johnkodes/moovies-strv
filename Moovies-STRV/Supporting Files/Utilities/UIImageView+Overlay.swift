//
//  UIImageOverlay.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 26/08/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    
    func setupImageOverlay() {
        
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = [UIColor.clear.cgColor, UIColor.init(named: "dark")?.cgColor ?? UIColor.darkGray]
        
        let overlay: UIView = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height / 2))
        
        overlay.layer.insertSublayer(gradient, at: 0)
        
        self.addSubview(overlay)
    }
}
