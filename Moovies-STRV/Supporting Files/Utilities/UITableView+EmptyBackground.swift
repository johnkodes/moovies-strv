//
//  UITableView.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 12/09/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    
    func setupEmptyBackgroundView() {
        
        let emptyBackgroundView = EmptyBackgroundView()
        self.backgroundView = emptyBackgroundView
        
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
}
