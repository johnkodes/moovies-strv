//
//  Date.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 15/09/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import Foundation

extension Date {
    
    func dateToString() -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        return dateFormatter.string(from: self)
    }
    
    func getYear() -> String {
        return "\(Calendar.current.component(.year, from: self))"
    }
    
    func getDate() -> String {
        return self.dateToString()
    }
    
    func twoMonthAgo() -> String {
        return (Calendar.current.date(byAdding: .month, value: -2, to: self)?.dateToString() ?? "")
    }

}
