//
//  CircleRatingView.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 28/07/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import UIKit

@IBDesignable class CircleRatingView: UIView {
    
    var timer = Timer()
    var isTimerRunning = false
    var currentNumber = 0
    
    let green = UIColor.init(named: "green")
    let yellow = UIColor.init(named: "yellow")
    let red = UIColor.init(named: "red")

    var percentage: CGFloat = 0.0 {
        didSet {
            percentage *= 10
            self.isHidden = percentage == 0
        }
    }
    
    func showRating(animated: Bool) {
        
        if animated {
            animateCircle(duration: 1)
            runTimer()
        } else {
            //label.text = "\(Int(percentage))%"
        }
    }

    @objc func updateTimer() {
        isTimerRunning = currentNumber < Int(percentage)
        
        if isTimerRunning {
            currentNumber += 1
            label.text = "\(Int(currentNumber))%"
            updateColor(rating: currentNumber)
        } else {
            timer.invalidate()
        }
    }
    
    private func updateColor(rating: Int) {
        switch rating {
        case 0...40:
            label.textColor = red
            circleLayer.strokeColor = red?.cgColor
            shadowCircleLayer.strokeColor = red?.withAlphaComponent(0.15).cgColor
        case 41...70:
            label.textColor = yellow
            circleLayer.strokeColor = yellow?.cgColor
            shadowCircleLayer.strokeColor = yellow?.withAlphaComponent(0.15).cgColor
        case 71...100:
            label.textColor = green
            circleLayer.strokeColor = green?.cgColor
            shadowCircleLayer.strokeColor = green?.withAlphaComponent(0.15).cgColor
        default:
            label.textColor = UIColor.green
        }
        
    }
    
    private func runTimer() {
        let duration = 1.0
        
        timer = Timer.scheduledTimer(timeInterval: duration / Double(percentage), target: self, selector: #selector(CircleRatingView.updateTimer), userInfo: nil, repeats: true)
    }
    
    var label: UILabel!
    
    var circleLayer: CAShapeLayer!
    var shadowCircleLayer: CAShapeLayer!
    
    override func draw(_ rect: CGRect) {
        layer.backgroundColor = UIColor.clear.cgColor
        
        let width = self.bounds.width
        let height = self.bounds.height
        
        self.shadowCircleLayer = CAShapeLayer()
        
        self.circleLayer = CAShapeLayer()
        circleLayer.frame = CGRect(x: 0, y: 0, width: width, height: height)
        
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: frame.size.width / 2.0, y: frame.size.height / 2.0), radius: (frame.size.width - 10)/2, startAngle: CGFloat(-90).degreesToRadians, endAngle: CGFloat(270).degreesToRadians, clockwise: true)
        
        circleLayer.path = circlePath.cgPath
        circleLayer.fillColor = UIColor.clear.cgColor
        circleLayer.strokeColor = UIColor.red.cgColor
        circleLayer.lineWidth = 3.0;
        
        // Don't draw the circle initially
        circleLayer.strokeEnd = percentage / 100
        
        shadowCircleLayer.path = circlePath.cgPath
        shadowCircleLayer.fillColor = UIColor.clear.cgColor
        shadowCircleLayer.strokeColor = UIColor.red.withAlphaComponent(0.15).cgColor
        shadowCircleLayer.lineWidth = 3.0;
        shadowCircleLayer.strokeEnd = 1.0
        
        label = UILabel()
        label.font = UIFont(name: "Calibre-Medium", size: 16)
        label.frame = CGRect(x: circleLayer.frame.origin.x + 1, y: circleLayer.frame.origin.y + 2, width: circleLayer.bounds.width, height: circleLayer.bounds.height)
        
        label.text = "\(Int(percentage))%"
        label.textColor = UIColor.red
        label.isHidden = false
        label.textAlignment = NSTextAlignment.center
        
        layer.addSublayer(shadowCircleLayer)
        layer.addSublayer(circleLayer)
        
        self.addSubview(label)
        
        updateColor(rating: Int(percentage))
    }
    
 
    
    private func animateCircle(duration: TimeInterval) {
        // We want to animate the strokeEnd property of the circleLayer
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        
        // Set the animation duration appropriately
        animation.duration = duration
        
        // Animate from 0 (no circle) to 1 (full circle)
        animation.fromValue = 0
        animation.toValue = percentage / 100
        
        // Do a linear animation (i.e. the speed of the animation stays the same)
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        
        //circleLayer.strokeEnd = percentage / 100
        circleLayer.strokeEnd = percentage / 100
        circleLayer.add(animation, forKey: "animateCircle")
    }
    
}

extension CGFloat {
    var degreesToRadians : CGFloat {
            return CGFloat(self) * CGFloat(Double.pi) / 180.0
    }
}
