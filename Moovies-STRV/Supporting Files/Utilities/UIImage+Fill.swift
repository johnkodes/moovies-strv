//
//  UIImage+Fill.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 14/09/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    
    class func image(with color: UIColor) -> UIImage {
        let rect = CGRect(origin: CGPoint(x: 0, y:0), size: CGSize(width: 1, height: 1))
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()!
        
        context.setFillColor(color.cgColor)
        context.fill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
}
