//
//  AlertPresenter.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 19/09/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import Foundation

import UIKit

public protocol AlertPresenting {
    func presentAlertController(controller : UIAlertController, fromController : UIViewController)
}

struct AlertPresenter : AlertPresenting {
    func presentAlertController(controller: UIAlertController, fromController: UIViewController) {
        fromController.present(controller, animated: true, completion: nil)
        //show.(controller, sender: nil)
    }
}

