//
//  GalleryCollectionViewCell.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 02/08/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import UIKit
import AlamofireImage

class GalleryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var image: UIImageView!
    
    func setupWithData(data: GalleryListItem) {
        
        let filter = AspectScaledToFillSizeFilter(
            size: image.frame.size
        )
        
        if let img = data.image {
            image.af_setImage(
                withURL: img.url(width: image.frame.width),
                placeholderImage: #imageLiteral(resourceName: "placeholderGallery"),
                filter: filter,
                imageTransition: .crossDissolve(0.2),
                completion: nil)
        } else {
            image.image = #imageLiteral(resourceName: "placeholderGallery")
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        image.af_cancelImageRequest()
        image.image = nil
    }

}
