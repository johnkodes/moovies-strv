//
//  MovieCollectionViewCell.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 01/08/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import UIKit
import AlamofireImage

class MovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    
    func setupWithData(data: MovieListItem) {
        
        let filter = AspectScaledToFillSizeFilter(
            size: image.frame.size
        )
        
        if let img = data.image {

            image.af_setImage(
                withURL: img.url(width: image.frame.width),
                placeholderImage: #imageLiteral(resourceName: "placeholder-movie"),
                filter: filter,
                imageTransition: .crossDissolve(0.3),
                completion: nil)
            
        } else {
            image.image = #imageLiteral(resourceName: "placeholder-movie")
        }
    }
    
    override func prepareForReuse() {
        image.af_cancelImageRequest()
        image.image = nil
    }
    
}
