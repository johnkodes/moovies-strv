//
//  PersonCollectionViewCell.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 02/08/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import UIKit
import AlamofireImage

class PersonCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var movies: UILabel!
    
    func setupWithData(data: ActorListItem) {
        
        movies.adjustsFontSizeToFitWidth = false
        movies.lineBreakMode = .byTruncatingTail;
        
        name.text = data.name
        
        if let character = data.character {
            movies.text = character
        }
        
        if let movs = data.movies {
             movies.text = movs.joined(separator: ", ")
        }
        
        let filter = AspectScaledToFillSizeFilter(
            size: image.frame.size
        )
        
        if let img = data.image {
            image.af_setImage(
                withURL: img.url(width: image.frame.width),
                placeholderImage: #imageLiteral(resourceName: "placeholderPerson"),
                filter: filter,
                imageTransition: .crossDissolve(0.2),
                completion: nil)
        } else {
            image.image = #imageLiteral(resourceName: "placeholderPerson")
        }
    }

}
