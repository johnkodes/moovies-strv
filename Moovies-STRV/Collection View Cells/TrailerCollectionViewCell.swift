//
//  TrailerCollectionViewCell.swift
//  Moovies-STRV
//
//  Created by Jan Kodeš on 08/08/2017.
//  Copyright © 2017 Jan Kodeš. All rights reserved.
//

import UIKit
import AlamofireImage

class TrailerCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var image: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupWithData(data: TrailerListItem) {
        
        let filter = AspectScaledToFillSizeFilter(
            size: image.frame.size
        )
        
        image.af_setImage(
            withURL: data.imageURL,
            placeholderImage: #imageLiteral(resourceName: "placeholder-movie"),
            filter: filter,
            imageTransition: .crossDissolve(0.2),
            completion: nil)
    }

}
